﻿<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\City;
use backend\models\User;


/**
 * User controller
 */
class UserController extends Controller
{

    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        echo "User Controller";
        exit();
    }

    public function actionLogin()
    {
        $email = $_GET['email'];
        $password = $_GET['password'];
        if (User::find()->where(['email' => $email])
            ->andWhere(['password' => $password])
            ->andWhere(['is_archived' => 0])
            ->exists())
        {
            $model = User::find()->where(['email' => $email])
            ->andWhere(['password' => $password])
            ->andWhere(['is_archived' => 0])
            ->one();
             $response = array(
                    "user_id" => $model->id,
                    "error"=>0
                    );
                    echo json_encode($response);
        }
        else
        {
            $response = array(
                    "user_id" => 0,
                    "error"=>1
                    );
                    echo json_encode($response);
        }
        
    }

    public function actionGetAllUsers()
    {
        $response = [];
        $response['user'] = User::find()->where(['is_archived'=>0])->asArray()->all();
        echo json_encode($response);
    }

    public function actionGetUserById()
    {
        $id = $_GET['id'];
        $response = User::find()->where(['id'=>$_GET['id']])
        ->andWhere(['is_archived' => 0])
        ->asArray()->one();
        echo json_encode($response);
    }

    public function actionGetUserByName()
    {
        $response = User::find()->where(['fname'=>$_GET['fname']])
        ->andWhere(['lname' => $_GET['lname']])
        ->andWhere(['is_archived' => 0])
        ->asArray()->one();
        echo json_encode($response);
    }

    public function actionAddUser()
    {
        $model = new User();
        $model->attributes = $_POST["User"];
        if ($model->save()) 
        {
            $response = array(
                    "user_id" => $model->id,
                    "error"=>0
                    );
                    echo json_encode($response);
        } 
        else 
        {
            $response = array(
                    "user_id" =>0,
                    "error"=>1,
                    "error_msg" =>$model->getErrors()
                    );
            echo json_encode($response);
        }
        //echo json_encode($response);
    }


    public function actionEditUser()
    {
        $id = $_GET['id'];
        $model = User::find()->where(['id' => $id])->one();
        if(isset($_POST["User"]))
        {
            $model->attributes = $_POST["User"];
            if ($model->save()) 
            {
              $this->redirect(\Yii::$app->urlManager->createUrl("user/get-user-by-id?id=".$model->id));
            } 
            else 
            {
                echo json_encode($model->getErrors());
            }
        }
        
    }

    public function actionDeleteUser()
    {
        $id = $_GET['id'];
        $model = User::find()->where(['id' => $id])->one();
        $model->is_archived = 1 ;
        if ($model->save()) 
        {
            $this->redirect(\Yii::$app->urlManager->createUrl("user/get-all-users"));
        } 
        else 
        {
            echo json_encode($model->getErrors());
        }
        
    }


   


}
