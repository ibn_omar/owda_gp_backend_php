<?php

use yii\db\Migration;

class m170412_131702_currency_seeding extends Migration
{
    public function safeUp()
    {
        $this->insert('currency', ['id' =>'1', 'iso_code' => 'EGP', 'symbol' => 'E£', 'uni_code' => 'E£', 'position' => 'after']);
        $this->insert('currency', ['id' =>'2', 'iso_code' => 'USD', 'symbol' => '$', 'uni_code' => '$', 'position' => 'before']);
        $this->insert('currency', ['id' =>'3', 'iso_code' => 'EUR', 'symbol' => '€', 'uni_code' => '€', 'position' => 'before']);
        $this->insert('currency', ['id' =>'4', 'iso_code' => 'GBP', 'symbol' => '£', 'uni_code' => '£', 'position' => 'before']);
        $this->insert('currency', ['id' =>'5', 'iso_code' => 'AED', 'symbol' => 'إ.د', 'uni_code' => '', 'position' => 'after']);
    }

    public function safeDown()
    {
        echo "m170412_131702_currency_seeding cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
