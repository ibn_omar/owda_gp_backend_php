<?php

use yii\db\Migration;

class m170410_134739_city_region_seeding extends Migration
{
    public function safeUp()
    {
        //Cities
        $this->insert('city', ['id' => 1 ,'name' => 'Aswan']);
        $this->insert('city', ['id' => 2 ,'name' => 'Asyut']);
        $this->insert('city', ['id' => 3 ,'name' => 'Bani Suwayf']);
        $this->insert('city', ['id' => 4 ,'name' => 'Bur Said']);
        $this->insert('city', ['id' => 5 ,'name' => 'Cairo']);
        $this->insert('city', ['id' => 6 ,'name' => 'Dumyat']);
        $this->insert('city', ['id' => 7 ,'name' => 'Kafr-ash-Shaykh']);
        $this->insert('city', ['id' => 8 ,'name' => 'Matruh']);
        $this->insert('city', ['id' => 9 ,'name' => 'Muhafazat ad Daqahliyah']);
        $this->insert('city', ['id' => 10 ,'name' => 'Muhafazat al Fayyum']);
        $this->insert('city', ['id' => 11 ,'name' => 'Muhafazat al Gharbiyah']);
        $this->insert('city', ['id' => 12 ,'name' => 'Muhafazat al Iskandariyah']);
        $this->insert('city', ['id' => 13 ,'name' => 'Muhafazat al Qahirah']);
        $this->insert('city', ['id' => 14 ,'name' => 'Qina']);
        $this->insert('city', ['id' => 15 ,'name' => 'Sawhaj']);
        $this->insert('city', ['id' => 16 ,'name' => 'Sina al-Janubiyah']);
        $this->insert('city', ['id' => 17 ,'name' => 'Sina ash-Shamaliyah']);
        $this->insert('city', ['id' => 18 ,'name' => 'ad-Daqahliyah']);
        $this->insert('city', ['id' => 19 ,'name' => 'al-Bahr-al-Ahmar']);
        $this->insert('city', ['id' => 20 ,'name' => 'al-Buhayrah']);
        $this->insert('city', ['id' => 21 ,'name' => 'al-Fayyum']);
        $this->insert('city', ['id' => 22 ,'name' => 'al-Gharbiyah']);
        $this->insert('city', ['id' => 23 ,'name' => 'al-Iskandariyah']);
        $this->insert('city', ['id' => 24 ,'name' => 'al-Ismailiyah']);
        $this->insert('city', ['id' => 25 ,'name' => 'al-Jizah']);
        $this->insert('city', ['id' => 26 ,'name' => 'al-Minufiyah']);
        $this->insert('city', ['id' => 27 ,'name' => 'al-Minya']);
        $this->insert('city', ['id' => 28 ,'name' => 'al-Qahira']);
        $this->insert('city', ['id' => 29 ,'name' => 'al-Qalyubiyah']);
        $this->insert('city', ['id' => 30 ,'name' => 'al-Uqsur']);
        $this->insert('city', ['id' => 31 ,'name' => 'al-Wadi al-Jadid']);
        $this->insert('city', ['id' => 32 ,'name' => 'as-Suways']);
        $this->insert('city', ['id' => 33 ,'name' => 'ash-Sharqiyah']);

        //Regions
        $this->insert('region', ['id' => 1 ,'city_id' => 1,'name' => 'Aswan']);
        $this->insert('region', ['id' => 2 ,'city_id' => 1,'name' => 'Daraw']);
        $this->insert('region', ['id' => 3 ,'city_id' => 1,'name' => 'Kawm Umbu']);
        $this->insert('region', ['id' => 4 ,'city_id' => 1,'name' => 'an-Nasir']);

        $this->insert('region', ['id' => 5 ,'city_id' => 2,'name' => 'Abnub']);
        $this->insert('region', ['id' => 6 ,'city_id' => 2,'name' => 'Abu Tij']);
        $this->insert('region', ['id' => 7 ,'city_id' => 2,'name' => 'Asyut']);
        $this->insert('region', ['id' => 8 ,'city_id' => 2,'name' => 'Bani Muhammadiyat']);
        $this->insert('region', ['id' => 9 ,'city_id' => 2,'name' => 'Dayrut']);
        $this->insert('region', ['id' => 10 ,'city_id' => 2,'name' => 'Dayrut-ash-Sharif']);
        $this->insert('region', ['id' => 11 ,'city_id' => 2,'name' => 'Manfalut']);
        $this->insert('region', ['id' => 12 ,'city_id' => 2,'name' => 'Musha']);
        $this->insert('region', ['id' => 13 ,'city_id' => 2,'name' => 'Sahil Salim']);
        $this->insert('region', ['id' => 14 ,'city_id' => 2,'name' => 'Sanabu']);
        $this->insert('region', ['id' => 15 ,'city_id' => 2,'name' => 'Umm-al-Qusur']);
        $this->insert('region', ['id' => 16 ,'city_id' => 2,'name' => 'al-Badari']);
        $this->insert('region', ['id' => 17 ,'city_id' => 2,'name' => 'al-Qusiyah']);
        $this->insert('region', ['id' => 18 ,'city_id' => 2,'name' => 'an-Nukhaylah']);

        $this->insert('region', ['id' => 19 ,'city_id' => 5,'name' => '6th of October City']);
        $this->insert('region', ['id' => 20 ,'city_id' => 5,'name' => 'Ataba']);
        $this->insert('region', ['id' => 21 ,'city_id' => 5,'name' => 'Cairo']);
        $this->insert('region', ['id' => 22 ,'city_id' => 5,'name' => 'Nasr']);
        $this->insert('region', ['id' => 23 ,'city_id' => 5,'name' => 'Nasr City']);
        $this->insert('region', ['id' => 24 ,'city_id' => 5,'name' => 'Obour City']);

        //6
        $this->insert('region', ['id' => 25 ,'city_id' => 6,'name' => 'Izbat-al-Burj']);
        $this->insert('region', ['id' => 26 ,'city_id' => 6,'name' => 'Damietta']);
        $this->insert('region', ['id' => 27 ,'city_id' => 6,'name' => 'Dumyat']);
        $this->insert('region', ['id' => 28 ,'city_id' => 6,'name' => 'El-Zarka']);
        $this->insert('region', ['id' => 29 ,'city_id' => 6,'name' => 'Faraskur']);
        $this->insert('region', ['id' => 30 ,'city_id' => 6,'name' => 'Kafr Saad']);
        $this->insert('region', ['id' => 31 ,'city_id' => 6,'name' => 'Kafr-al-Battikh']);
        $this->insert('region', ['id' => 32 ,'city_id' => 6,'name' => 'az-Zarqa']);

        //7
        $this->insert('region', ['id' => 33 ,'city_id' => 7,'name' => 'Biyala']);
        $this->insert('region', ['id' => 34 ,'city_id' => 7,'name' => 'Disuq']);
        $this->insert('region', ['id' => 35 ,'city_id' => 7,'name' => 'Fuwah']);
        $this->insert('region', ['id' => 36 ,'city_id' => 7,'name' => 'Kafr-al-Jaraidah']);
        $this->insert('region', ['id' => 37 ,'city_id' => 7,'name' => 'Kafr-ash-Shaykh']);
        $this->insert('region', ['id' => 38 ,'city_id' => 7,'name' => 'Mutubis']);
        $this->insert('region', ['id' => 39 ,'city_id' => 7,'name' => 'Qallin']);
        $this->insert('region', ['id' => 40 ,'city_id' => 7,'name' => 'Sidi Salim']);
        $this->insert('region', ['id' => 41 ,'city_id' => 7,'name' => 'al-Burj']);
        $this->insert('region', ['id' => 42 ,'city_id' => 7,'name' => 'al-Burullus']);
        $this->insert('region', ['id' => 43 ,'city_id' => 7,'name' => 'al-Haddadi']);
        $this->insert('region', ['id' => 44 ,'city_id' => 7,'name' => 'al-Hamul']);

        //8
        $this->insert('region', ['id' => 45 ,'city_id' => 8,'name' => 'Marsa Matruh']);
        $this->insert('region', ['id' => 46 ,'city_id' => 8,'name' => 'Nasr']);
        $this->insert('region', ['id' => 47 ,'city_id' => 8,'name' => 'Sidi Barrani']);
        $this->insert('region', ['id' => 48 ,'city_id' => 8,'name' => 'Zawiyat Shammas']);
        $this->insert('region', ['id' => 49 ,'city_id' => 8,'name' => 'ad-Dabaa']);

        //14
        $this->insert('region', ['id' => 50 ,'city_id' => 14,'name' => 'Armant']);
        $this->insert('region', ['id' => 51 ,'city_id' => 14,'name' => 'Asfun-al-Matainah']);
        $this->insert('region', ['id' => 52 ,'city_id' => 14,'name' => 'Dandarah']);
        $this->insert('region', ['id' => 53 ,'city_id' => 14,'name' => 'Dishna']);
        $this->insert('region', ['id' => 54 ,'city_id' => 14,'name' => 'Farshut']);
        $this->insert('region', ['id' => 55 ,'city_id' => 14,'name' => 'Hijazah']);
        $this->insert('region', ['id' => 56 ,'city_id' => 14,'name' => 'Hiw']);
        $this->insert('region', ['id' => 57 ,'city_id' => 14,'name' => 'Idfu']);
        $this->insert('region', ['id' => 58 ,'city_id' => 14,'name' => 'Isna']);
        $this->insert('region', ['id' => 59 ,'city_id' => 14,'name' => 'Kiman-al-Matainah']);
        $this->insert('region', ['id' => 60 ,'city_id' => 14,'name' => 'Naj Hammadi']);
        $this->insert('region', ['id' => 61 ,'city_id' => 14,'name' => 'Naqadah']);
        $this->insert('region', ['id' => 62 ,'city_id' => 14,'name' => 'Qift']);
        $this->insert('region', ['id' => 63 ,'city_id' => 14,'name' => 'Qina']);
        $this->insert('region', ['id' => 64 ,'city_id' => 14,'name' => 'Qus']);
        $this->insert('region', ['id' => 65 ,'city_id' => 14,'name' => 'ad-Dabbiyah']);
        $this->insert('region', ['id' => 66 ,'city_id' => 14,'name' => 'ad-Dayr']);
        $this->insert('region', ['id' => 67 ,'city_id' => 14,'name' => 'al-Ballas']);
        $this->insert('region', ['id' => 68 ,'city_id' => 14,'name' => 'al-Karnak']);
        $this->insert('region', ['id' => 69 ,'city_id' => 14,'name' => 'al-Waqf']);
        $this->insert('region', ['id' => 70 ,'city_id' => 14,'name' => 'ar-Radisiyat-al-Bahriyah']);

        //15
        $this->insert('region', ['id' => 71 ,'city_id' => 15,'name' => 'Akhmim']);
        $this->insert('region', ['id' => 72 ,'city_id' => 15,'name' => 'Awlad Tawq Sharq']);
        $this->insert('region', ['id' => 73 ,'city_id' => 15,'name' => 'Dar-as-Salam']);
        $this->insert('region', ['id' => 74 ,'city_id' => 15,'name' => 'Jirja']);
        $this->insert('region', ['id' => 75 ,'city_id' => 15,'name' => 'Juhaynah']);
        $this->insert('region', ['id' => 76 ,'city_id' => 15,'name' => 'Sawhaj']);
        $this->insert('region', ['id' => 77 ,'city_id' => 15,'name' => 'Tahta']);
        $this->insert('region', ['id' => 78 ,'city_id' => 15,'name' => 'Tima']);
        $this->insert('region', ['id' => 79 ,'city_id' => 15,'name' => 'al-Balyana']);
        $this->insert('region', ['id' => 80 ,'city_id' => 15,'name' => 'al-Manshah']);
        $this->insert('region', ['id' => 81 ,'city_id' => 15,'name' => 'al-Maragah']);

        //18
        $this->insert('region', ['id' => 82 ,'city_id' => 18,'name' => 'Aja']);
        $this->insert('region', ['id' => 83 ,'city_id' => 18,'name' => 'Bahut']);
        $this->insert('region', ['id' => 84 ,'city_id' => 18,'name' => 'Bilqas']);
        $this->insert('region', ['id' => 85 ,'city_id' => 18,'name' => 'Dikirnis']);
        $this->insert('region', ['id' => 86 ,'city_id' => 18,'name' => 'Minyat-an-Nasr']);
        $this->insert('region', ['id' => 87 ,'city_id' => 18,'name' => 'Mit Gamr']);
        $this->insert('region', ['id' => 88 ,'city_id' => 18,'name' => 'Shirbin']);
        $this->insert('region', ['id' => 89 ,'city_id' => 18,'name' => 'Talkha']);
        $this->insert('region', ['id' => 90 ,'city_id' => 18,'name' => 'al-Jamaliyah']);
        $this->insert('region', ['id' => 91 ,'city_id' => 18,'name' => 'al-Maasarah']);
        $this->insert('region', ['id' => 92 ,'city_id' => 18,'name' => 'al-Mansurah']);
        $this->insert('region', ['id' => 93 ,'city_id' => 18,'name' => 'al-Manzilah']);
        $this->insert('region', ['id' => 94 ,'city_id' => 18,'name' => 'al-Matariyah']);
        $this->insert('region', ['id' => 95 ,'city_id' => 18,'name' => 'as-Sinbillawayn']);

        //19
        $this->insert('region', ['id' => 96 ,'city_id' => 19,'name' => 'Ras Gharib']);
        $this->insert('region', ['id' => 97 ,'city_id' => 19,'name' => 'Safaja']);
        $this->insert('region', ['id' => 98 ,'city_id' => 19,'name' => 'al-Ghardaqah']);
        $this->insert('region', ['id' => 99 ,'city_id' => 19,'name' => 'al-Qusayr']);

        //20
        $this->insert('region', ['id' => 100 ,'city_id' => 20,'name' => 'Abu Hummus']);
        $this->insert('region', ['id' => 101 ,'city_id' => 20,'name' => 'Abu al-Matamir']);
        $this->insert('region', ['id' => 102 ,'city_id' => 20,'name' => 'Buturis']);
        $this->insert('region', ['id' => 103 ,'city_id' => 20,'name' => 'Damanhur']);
        $this->insert('region', ['id' => 104 ,'city_id' => 20,'name' => 'Edfina']);
        $this->insert('region', ['id' => 105 ,'city_id' => 20,'name' => 'Hawsh Isa']);
        $this->insert('region', ['id' => 106 ,'city_id' => 20,'name' => 'Idku']);
        $this->insert('region', ['id' => 107 ,'city_id' => 20,'name' => 'Ityay-al-Barud']);
        $this->insert('region', ['id' => 108 ,'city_id' => 20,'name' => 'Kafr Salim']);
        $this->insert('region', ['id' => 109 ,'city_id' => 20,'name' => 'Kafr-ad-Dawwar']);
        $this->insert('region', ['id' => 110 ,'city_id' => 20,'name' => 'Kawm Hamada']);
        $this->insert('region', ['id' => 111 ,'city_id' => 20,'name' => 'Nubaria']);
        $this->insert('region', ['id' => 112 ,'city_id' => 20,'name' => 'Rashid']);
        $this->insert('region', ['id' => 113 ,'city_id' => 20,'name' => 'Shubra Khit']);
        $this->insert('region', ['id' => 114 ,'city_id' => 20,'name' => 'Zawiyat Sidi Gazi']);
        $this->insert('region', ['id' => 115 ,'city_id' => 20,'name' => 'ad-Dilinjat']);
        $this->insert('region', ['id' => 116 ,'city_id' => 20,'name' => 'al-Kawm-al-Akhdar']);
        $this->insert('region', ['id' => 117 ,'city_id' => 20,'name' => 'al-Mahmudiyah']);
        $this->insert('region', ['id' => 118 ,'city_id' => 20,'name' => 'ar-Rahmaniyah']);

        //21
        $this->insert('region', ['id' => 119 ,'city_id' => 21,'name' => 'Fidimin']);
        $this->insert('region', ['id' => 120 ,'city_id' => 21,'name' => 'Ibshaway']);
        $this->insert('region', ['id' => 121 ,'city_id' => 21,'name' => 'Itsa']);
        $this->insert('region', ['id' => 122 ,'city_id' => 21,'name' => 'Qasr Qarun']);
        $this->insert('region', ['id' => 123 ,'city_id' => 21,'name' => 'Sanhur']);
        $this->insert('region', ['id' => 124 ,'city_id' => 21,'name' => 'Sinnuris']);
        $this->insert('region', ['id' => 125 ,'city_id' => 21,'name' => 'Tamiyah']);
        $this->insert('region', ['id' => 126 ,'city_id' => 21,'name' => 'al-Fayyum']);

        //22
        $this->insert('region', ['id' => 127 ,'city_id' => 22,'name' => 'Abyar']);
        $this->insert('region', ['id' => 128 ,'city_id' => 22,'name' => 'Basyun']);
        $this->insert('region', ['id' => 129 ,'city_id' => 22,'name' => 'Kafr-az-Zayyat']);
        $this->insert('region', ['id' => 130 ,'city_id' => 22,'name' => 'Mahallat Marhum']);
        $this->insert('region', ['id' => 131 ,'city_id' => 22,'name' => 'Nisf Thani Bashbish']);
        $this->insert('region', ['id' => 132 ,'city_id' => 22,'name' => 'Qutur']);
        $this->insert('region', ['id' => 133 ,'city_id' => 22,'name' => 'Samannud']);
        $this->insert('region', ['id' => 134 ,'city_id' => 22,'name' => 'Tanta']);
        $this->insert('region', ['id' => 135 ,'city_id' => 22,'name' => 'Zifta']);
        $this->insert('region', ['id' => 136 ,'city_id' => 22,'name' => 'ad-Daljamun']);
        $this->insert('region', ['id' => 137 ,'city_id' => 22,'name' => 'al-Mahallah al-Kubra']);
        $this->insert('region', ['id' => 138 ,'city_id' => 22,'name' => 'as-Santah']);

        //23
        $this->insert('region', ['id' => 139 ,'city_id' => 23,'name' => 'Agamy']);
        $this->insert('region', ['id' => 140 ,'city_id' => 23,'name' => 'al-Iskandariyah']);
        $this->insert('region', ['id' => 141 ,'city_id' => 23,'name' => 'al-Maks']);

        //24
        $this->insert('region', ['id' => 142 ,'city_id' => 24,'name' => 'Faid']);
        $this->insert('region', ['id' => 143 ,'city_id' => 24,'name' => 'Sarabiyum']);
        $this->insert('region', ['id' => 144 ,'city_id' => 24,'name' => 'al-Ismailiyah']);

        //25
        $this->insert('region', ['id' => 145 ,'city_id' => 25,'name' => 'Atfih']);
        $this->insert('region', ['id' => 146 ,'city_id' => 25,'name' => 'Awsim']);
        $this->insert('region', ['id' => 147 ,'city_id' => 25,'name' => 'Giza']);
        $this->insert('region', ['id' => 148 ,'city_id' => 25,'name' => 'Madinat Sittah Uktubar']);
        $this->insert('region', ['id' => 149 ,'city_id' => 25,'name' => 'Nahya']);
        $this->insert('region', ['id' => 150 ,'city_id' => 25,'name' => 'Saqqarah']);
        $this->insert('region', ['id' => 151 ,'city_id' => 25,'name' => 'al-Ayyat']);
        $this->insert('region', ['id' => 152 ,'city_id' => 25,'name' => 'al-Badrashayn']);
        $this->insert('region', ['id' => 153 ,'city_id' => 25,'name' => 'al-Hawamidiyah']);
        $this->insert('region', ['id' => 154 ,'city_id' => 25,'name' => 'al-Jizah']);
        $this->insert('region', ['id' => 155 ,'city_id' => 25,'name' => 'al-Mansuriyah']);
        $this->insert('region', ['id' => 156 ,'city_id' => 25,'name' => 'al-Wahat-al-Bahriyah']);
        $this->insert('region', ['id' => 157 ,'city_id' => 25,'name' => 'as-Saff']);

        //26
        $this->insert('region', ['id' => 158 ,'city_id' => 26,'name' => 'Ashmun']);
        $this->insert('region', ['id' => 159 ,'city_id' => 26,'name' => 'Birkat-as-Sab']);
        $this->insert('region', ['id' => 160 ,'city_id' => 26,'name' => 'Milij']);
        $this->insert('region', ['id' => 161 ,'city_id' => 26,'name' => 'Minuf']);
        $this->insert('region', ['id' => 162 ,'city_id' => 26,'name' => 'Quwaysina']);
        $this->insert('region', ['id' => 163 ,'city_id' => 26,'name' => 'Shibin-al-Kawm']);
        $this->insert('region', ['id' => 164 ,'city_id' => 26,'name' => 'Sirs-al-Layyanah']);
        $this->insert('region', ['id' => 165 ,'city_id' => 26,'name' => 'Tala']);
        $this->insert('region', ['id' => 166 ,'city_id' => 26,'name' => 'al-Bajur']);
        $this->insert('region', ['id' => 167 ,'city_id' => 26,'name' => 'al-Batanun']);
        $this->insert('region', ['id' => 168 ,'city_id' => 26,'name' => 'ash-Shuhada']);

        //27
        $this->insert('region', ['id' => 169 ,'city_id' => 27,'name' => 'Abu Qurqas']);
        $this->insert('region', ['id' => 170 ,'city_id' => 27,'name' => 'Bani Mazar']);
        $this->insert('region', ['id' => 171 ,'city_id' => 27,'name' => 'Dayr Mawas']);
        $this->insert('region', ['id' => 172 ,'city_id' => 27,'name' => 'Magagah']);
        $this->insert('region', ['id' => 173 ,'city_id' => 27,'name' => 'Mallawi']);
        $this->insert('region', ['id' => 174 ,'city_id' => 27,'name' => 'Matay']);
        $this->insert('region', ['id' => 175 ,'city_id' => 27,'name' => 'Samalut']);
        $this->insert('region', ['id' => 176 ,'city_id' => 27,'name' => 'Tallah']);
        $this->insert('region', ['id' => 177 ,'city_id' => 27,'name' => 'Tandah']);
        $this->insert('region', ['id' => 178 ,'city_id' => 27,'name' => 'al-Anayim']);
        $this->insert('region', ['id' => 179 ,'city_id' => 27,'name' => 'al-Fikriyah']);
        $this->insert('region', ['id' => 180 ,'city_id' => 27,'name' => 'al-Minya']);
        $this->insert('region', ['id' => 181 ,'city_id' => 27,'name' => 'ar-Rawdah']);

        //28
        $this->insert('region', ['id' => 182 ,'city_id' => 28,'name' => 'Badr City']);
        $this->insert('region', ['id' => 183 ,'city_id' => 28,'name' => 'Heliopolis']);
        $this->insert('region', ['id' => 184 ,'city_id' => 28,'name' => 'al-Qahira']);

        //29
        $this->insert('region', ['id' => 185 ,'city_id' => 29,'name' => 'Abu Zabal']);
        $this->insert('region', ['id' => 186 ,'city_id' => 29,'name' => 'Banha']);
        $this->insert('region', ['id' => 187 ,'city_id' => 29,'name' => 'Qalyub']);
        $this->insert('region', ['id' => 188 ,'city_id' => 29,'name' => 'Shubra al-Khaymah']);
        $this->insert('region', ['id' => 189 ,'city_id' => 29,'name' => 'Sibin-al-Qanatir']);
        $this->insert('region', ['id' => 190 ,'city_id' => 29,'name' => 'Tukh']);
        $this->insert('region', ['id' => 191 ,'city_id' => 29,'name' => 'al-Khankah']);
        $this->insert('region', ['id' => 192 ,'city_id' => 29,'name' => 'al-Qanatir-al-Khayriyah']);

        //30
        $this->insert('region', ['id' => 193 ,'city_id' => 30,'name' => 'al-Uqsur']);

        //32
        $this->insert('region', ['id' => 194 ,'city_id' => 32,'name' => 'as-Suways']);

        //33
        $this->insert('region', ['id' => 195 ,'city_id' => 33,'name' => 'Abu Hammad']);
        $this->insert('region', ['id' => 196 ,'city_id' => 33,'name' => 'Abu Kabir']);
        $this->insert('region', ['id' => 197 ,'city_id' => 33,'name' => 'Bilbays']);
        $this->insert('region', ['id' => 198 ,'city_id' => 33,'name' => 'Diyarb Najm']);
        $this->insert('region', ['id' => 199 ,'city_id' => 33,'name' => 'Faqus']);
        $this->insert('region', ['id' => 201 ,'city_id' => 33,'name' => 'Hihya']);
        $this->insert('region', ['id' => 202 ,'city_id' => 33,'name' => 'Kafr Saqr']);
        $this->insert('region', ['id' => 203 ,'city_id' => 33,'name' => 'Mashtul-as-Suq']);
        $this->insert('region', ['id' => 204 ,'city_id' => 33,'name' => 'Minyat-al-Qamh']);
        $this->insert('region', ['id' => 205 ,'city_id' => 33,'name' => 'al-Ashir mir-Ramadan']);
        $this->insert('region', ['id' => 206 ,'city_id' => 33,'name' => 'al-Husayniyah']);
        $this->insert('region', ['id' => 207 ,'city_id' => 33,'name' => 'al-Ibrahimiyah']);
        $this->insert('region', ['id' => 208 ,'city_id' => 33,'name' => 'al-Qanayat']);
        $this->insert('region', ['id' => 209 ,'city_id' => 33,'name' => 'al-Qassasin']);
        $this->insert('region', ['id' => 210 ,'city_id' => 33,'name' => 'al-Qurayn']);
        $this->insert('region', ['id' => 211 ,'city_id' => 33,'name' => 'as-Salihiyah']);
        $this->insert('region', ['id' => 212 ,'city_id' => 33,'name' => 'at-Tall-al-Kabir']);
        $this->insert('region', ['id' => 213 ,'city_id' => 33,'name' => 'az-Zaqaziq']);
    }

    public function safeDown()
    {
        echo "m170410_134739_city_region_seeding cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
