<?php

use yii\db\Migration;

class m170410_173428_pos_foriegn_keys extends Migration
{
    public function up()
    {
        // =====================================
        // ========  product Foreign Keys ======
        // =====================================

        $this->addForeignKey(
            'fk-product-brand_id',
            'product',
            'brand_id',
            'brand',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-product-category_id',
            'product',
            'category_id',
            'pos_product_category',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-product-unit_id',
            'product',
            'unit_id',
            'unit',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-product-created_by',
            'product',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );
        /// ============================================
        /// ========== offer foreign keys ==============
        /// ============================================
        
        $this->addForeignKey(
            'fk-pos_offer-created_by',
            'pos_offer',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_offer-product_id',
            'pos_offer',
            'product_id',
            'product',
            'id',
            'RESTRICT'
        );

        /// ==================================================
        /// ========== offer-store foreign keys ==============
        /// ==================================================
        
        $this->addForeignKey(
            'fk-pos_offer_store-offer_id',
            'pos_offer_store',
            'offer_id',
            'pos_offer',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_offer_store-store_id',
            'pos_offer_store',
            'store_id',
            'pos_store',
            'id',
            'RESTRICT'
        );
       
        // =======================================
        // ========  Order Foreign Keys ==========
        // =======================================

        $this->addForeignKey(
            'fk-pos_order-client_id',
            'pos_order',
            'client_id',
            'client',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_order-financial_year_id',
            'pos_order',
            'financial_year_id',
            'acc_financial_year',
            'id',
            'RESTRICT'
        );
                    
        $this->addForeignKey(
            'fk-pos_order-tree_item_currency_id',
            'pos_order',
            'tree_item_currency_id',
            'acc_tree_item_currency',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-pos_order-treasury_currency_id',
            'pos_order',
            'treasury_currency_id',
            'treasury_currency',
            'id',
            'RESTRICT'
        );
       
        $this->addForeignKey(
            'fk-pos_order-store_id',
            'pos_order',
            'store_id',
            'pos_store',
            'id',
            'RESTRICT'
        );


        $this->addForeignKey(
            'fk-pos_order-created_by',
            'pos_order',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );


        // ============================================
        // ========  Order-item Foreign Keys ==========
        // ============================================

        $this->addForeignKey(
            'fk-pos_order_item-order_id',
            'pos_order_item',
            'order_id',
            'pos_order',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_order_item-product_id',
            'pos_order_item',
            'product_id',
            'product',
            'id',
            'RESTRICT'
        );
                    

        // ==================================================
        // ========  Product-Category Foreign Keys ==========
        // ==================================================

        $this->addForeignKey(
            'fk-pos_product_category-parent_category_id',
            'pos_product_category',
            'parent_category_id',
            'pos_product_category',
            'id',
            'RESTRICT'
        );

        /// ===========================================================
        /// ========== Product Price Change foreign keys ==============
        /// ===========================================================
        
        $this->addForeignKey(
            'fk-pos_product_price_change-created_by',
            'pos_product_price_change',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_product_price_change-product_id',
            'pos_product_price_change',
            'product_id',
            'product',
            'id',
            'RESTRICT'
        );


        // ===============================================
        // ========  Store Product Foreign Keys ==========
        // ===============================================

        $this->addForeignKey(
            'fk-pos_store_product-store_id',
            'pos_store_product',
            'store_id',
            'pos_store',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_store_product-product_id',
            'pos_store_product',
            'product_id',
            'product',
            'id',
            'RESTRICT'
        );
                    

        // ==========================================
        // ========  Supplier Foreign Keys ==========
        // ==========================================

    

        $this->addForeignKey(
            'fk-pos_supplier-region_id',
            'pos_supplier',
            'region_id',
            'region',
            'id',
            'RESTRICT'
        );

        // =========================================================
        // ========  Supplier Treasury Paper Foreign Keys ==========
        // =========================================================

        $this->addForeignKey(
            'fk-pos_supplier_treasury_paper-financial_year_id',
            'pos_supplier_treasury_paper',
            'financial_year_id',
            'acc_financial_year',
            'id',
            'RESTRICT'
        );
                    
        $this->addForeignKey(
            'fk-pos_supplier_treasury_paper-tree_item_currency_id',
            'pos_supplier_treasury_paper',
            'tree_item_currency_id',
            'acc_tree_item_currency',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-pos_supplier_treasury_paper-treasury_currency_id',
            'pos_supplier_treasury_paper',
            'treasury_currency_id',
            'treasury_currency',
            'id',
            'RESTRICT'
        );
       
        $this->addForeignKey(
            'fk-pos_supplier_treasury_paper-supplier_id',
            'pos_supplier_treasury_paper',
            'supplier_id',
            'pos_supplier',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_supplier_treasury_paper-created_by',
            'pos_supplier_treasury_paper',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );


        // ==============================================
        // ========  Supply Order Foreign Keys ==========
        // ==============================================

        $this->addForeignKey(
            'fk-pos_supply_order-supplier_id',
            'pos_supply_order',
            'supplier_id',
            'pos_supplier',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_supply_order-store_id',
            'pos_supply_order',
            'store_id',
            'pos_store',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_supply_order-created_by',
            'pos_supply_order',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );


        // ===============================================
        // ========  Supply Order Item Foreign Keys ======
        // ===============================================

        $this->addForeignKey(
            'fk-pos_supply_order_item-supply_order_id',
            'pos_supply_order_item',
            'supply_order_id',
            'pos_supply_order',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_supply_order_item-product_id',
            'pos_supply_order_item',
            'product_id',
            'product',
            'id',
            'RESTRICT'
        );
                    
        
        // ===============================================
        // ========  Transfer Item Foreign Keys ==========
        // ===============================================

        $this->addForeignKey(
            'fk-pos_transfer_item-transfer_order_id',
            'pos_transfer_item',
            'transfer_order_id',
            'pos_transfer_order',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_transfer_item-product_id',
            'pos_transfer_item',
            'product_id',
            'product',
            'id',
            'RESTRICT'
        );
                    
        $this->addForeignKey(
            'fk-pos_transfer_item-unit_id',
            'pos_transfer_item',
            'unit_id',
            'unit',
            'id',
            'RESTRICT'
        );

        // ===============================================
        // ======== Transfer Order Foreign Keys ==========
        // ===============================================

        $this->addForeignKey(
            'fk-pos_transfer_order-store_from_id',
            'pos_transfer_order',
            'store_from_id',
            'pos_store',
            'id',
            'RESTRICT'
        );
    
       $this->addForeignKey(
            'fk-pos_transfer_order-store_to_id',
            'pos_transfer_order',
            'store_to_id',
            'pos_store',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-pos_transfer_order-created_by',
            'pos_transfer_order',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );

        

    }

    public function safeDown()
    {
        
        /// ===========================================
        /// ========== Cart foreign keys ==============
        /// ===========================================
        
        $this->dropForeignKey(
            'fk-pos_cart-user_id',
            'pos_cart'
        );

        $this->dropForeignKey(
            'fk-pos_cart-product_id',
            'pos_cart'
        );

        $this->dropForeignKey(
            'fk-pos_cart-unit_id',
            'pos_cart'
        );
        
        /// ============================================
        /// ========== offer foreign keys ==============
        /// ============================================
        
        $this->addForeignKey(
            'fk-pos_offer-created_by',
            'pos_offer'
        );

        $this->addForeignKey(
            'fk-pos_offer-product_id',
            'pos_offer'
        );
        
        /// ==================================================
        /// ========== offer-store foreign keys ==============
        /// ==================================================
        
        $this->dropForeignKey(
            'fk-pos_offer_store-offer_id',
            'pos_offer_store'
        );

        $this->dropForeignKey(
            'fk-pos_offer_store-store_id',
            'pos_offer_store'
        );

        // =======================================
        // ========  Order Foreign Keys ==========
        // =======================================

        $this->dropForeignKey(
            'fk-pos_order-client_id',
            'pos_order'
        );

        $this->dropForeignKey(
            'fk-pos_order-financial_year_id',
            'pos_order'
        );
                    
        $this->dropForeignKey(
            'fk-pos_order-tree_item_currency_id',
            'pos_order'
        );
                        
        $this->dropForeignKey(
            'fk-pos_order-treasury_currency_id',
            'pos_order'
        );
       
        $this->dropForeignKey(
            'fk-pos_order-store_id',
            'pos_order'
        );

        $this->dropForeignKey(
            'fk-pos_order-country_id',
            'pos_order'
        );
              
        $this->dropForeignKey(
            'fk-pos_order-city_id',
            'pos_order'
        );

        $this->dropForeignKey(
            'fk-pos_order-created_by',
            'pos_order'
        );
       
        // ============================================
        // ========  Order-item Foreign Keys ==========
        // ============================================

        $this->dropForeignKey(
            'fk-pos_order_item-order_id',
            'pos_order_item'
        );

        $this->dropForeignKey(
            'fk-pos_order_item-product_id',
            'pos_order_item'
        );
                    
        $this->dropForeignKey(
            'fk-pos_order_item-unit_id',
            'pos_order_item'
        );

        // ==================================================
        // ========  Product-Category Foreign Keys ==========
        // ==================================================

        $this->dropForeignKey(
            'fk-pos_product_category-parent_category_id',
            'pos_product_category'
        );

        /// ===========================================================
        /// ========== Product Price Change foreign keys ==============
        /// ===========================================================

        $this->addForeignKey(
            'fk-pos_product_price_change-created_by',
            'pos_product_price_change'
        );

        $this->addForeignKey(
            'fk-pos_product_price_change-product_id',
            'pos_product_price_change'
        );

        // ===============================================
        // ========  Store Product Foreign Keys ==========
        // ===============================================

        $this->dropForeignKey(
            'fk-pos_store_product-store_id',
            'pos_store_product'
        );

        $this->dropForeignKey(
            'fk-pos_store_product-product_id',
            'pos_store_product'
        );
                    
        $this->dropForeignKey(
            'fk-pos_store_product-unit_id',
            'pos_store_product'
        );

        // ==========================================
        // ========  Supplier Foreign Keys ==========
        // ==========================================

        $this->dropForeignKey(
            'fk-pos_supplier-country_id',
            'pos_supplier'
        );

        $this->dropForeignKey(
            'fk-pos_supplier-city_id',
            'pos_supplier'
        );
        
        // =========================================================
        // ========  Supplier Treasury Paper Foreign Keys ==========
        // =========================================================

        $this->dropForeignKey(
            'fk-pos_supplier_treasury_paper-financial_year_id',
            'pos_supplier_treasury_paper'
        );
                    
        $this->dropForeignKey(
            'fk-pos_supplier_treasury_paper-tree_item_currency_id',
            'pos_supplier_treasury_paper'
        );
                        
        $this->dropForeignKey(
            'fk-pos_supplier_treasury_paper-treasury_currency_id',
            'pos_supplier_treasury_paper'
        );
       
        $this->dropForeignKey(
            'fk-pos_supplier_treasury_paper-supplier_id',
            'pos_supplier_treasury_paper'
        );

        $this->dropForeignKey(
            'fk-pos_supplier_treasury_paper-created_by',
            'pos_supplier_treasury_paper'
        );

        // ==============================================
        // ========  Supply Order Foreign Keys ==========
        // ==============================================

        $this->dropForeignKey(
            'fk-pos_supply_order-supplier_id',
            'pos_supply_order'
        );

        $this->dropForeignKey(
            'fk-pos_supply_order-store_id',
            'pos_supply_order'
        );

        $this->dropForeignKey(
            'fk-pos_supply_order-created_by',
            'pos_supply_order'
        );
        
        // ===============================================
        // ========  Supply Order Item Foreign Keys ======
        // ===============================================

        $this->dropForeignKey(
            'fk-pos_supply_order_item-supply_order_id',
            'pos_supply_order_item'
        );

        $this->dropForeignKey(
            'fk-pos_supply_order_item-product_id',
            'pos_supply_order_item'
        );
                    
        $this->dropForeignKey(
            'fk-pos_supply_order_item-unit_id',
            'pos_supply_order_item'
        );

        // ===============================================
        // ========  Transfer Item Foreign Keys ==========
        // ===============================================

        $this->dropForeignKey(
            'fk-pos_transfer_item-transfer_order_id',
            'pos_transfer_item'
        );

        $this->dropForeignKey(
            'fk-pos_transfer_item-product_id',
            'pos_transfer_item'
        );
                    
        $this->dropForeignKey(
            'fk-pos_transfer_item-unit_id',
            'pos_transfer_item'
        );

        // ===============================================
        // ======== Transfer Order Foreign Keys ==========
        // ===============================================

        $this->dropForeignKey(
            'fk-pos_transfer_order-store_from_id',
            'pos_transfer_order'
        );

        $this->dropForeignKey(
            'fk-pos_transfer_order-store_to_id',
            'pos_transfer_order'
        );

        $this->dropForeignKey(
            'fk-pos_transfer_order-created_by',
            'pos_transfer_order'
        );

        // ==================================================
        // ========  User Preferences Foreign Keys ==========
        // ==================================================

        $this->dropForeignKey(
            'fk-pos_user_preferences-store_id',
            'pos_user_preferences'
        );

        $this->dropForeignKey(
            'fk-pos_user_preferences-treasury_id',
            'pos_user_preferences'
        );
                    
        $this->dropForeignKey(
            'fk-pos_user_preferences-user_id',
            'pos_user_preferences'
        );
        
        /// ========================================
        /// ========== Wishlist foreign keys =======
        /// ========================================
        
        $this->dropForeignKey(
            'fk-pos_wishlist-user_id',
            'pos_wishlist'
        );

        $this->dropForeignKey(
            'fk-pos_wishlist-product_id',
            'pos_wishlist'
        );
    }

    public function down()
    {
        echo "m170410_173428_pos_foriegn_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
