<?php

use yii\db\Migration;

class m170410_133907_accounting_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'iso_code' => $this->string()->notNull(),
            'symbol' => $this->string()->notNull(),
            'uni_code' => $this->string()->notNull(),
            'position' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%acc_cash_deposit}}', [
            'id' => $this->primaryKey(),
            'tree_item_currency_id' => $this->integer()->notNull(),
            'treasury_currency_id' => $this->integer()->notNull(),
            'financial_year_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'value' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'notes' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-acc_cash_deposit-tree_item_currency_id',
            'acc_cash_deposit',
            'tree_item_currency_id'
        );

        $this->createIndex(
            'idx-acc_cash_deposit-treasury_currency_id',
            'acc_cash_deposit',
            'treasury_currency_id'
        );
        
        $this->createIndex(
            'idx-acc_cash_deposit-financial_year_id',
            'acc_cash_deposit',
            'financial_year_id'
        );

        $this->createIndex(
            'idx-acc_cash_deposit-created_by',
            'acc_cash_deposit',
            'created_by'
        );

        $this->createTable('{{%acc_cash_order}}', [
            'id' => $this->primaryKey(),
            'tree_item_currency_id' => $this->integer()->notNull(),
            'treasury_currency_id' => $this->integer()->notNull(),
            'financial_year_id' => $this->integer()->notNull(),
            'cash_request_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'value' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'notes' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-acc_cash_order-tree_item_currency_id',
            'acc_cash_order',
            'tree_item_currency_id'
        );

        $this->createIndex(
            'idx-acc_cash_order-treasury_currency_id',
            'acc_cash_order',
            'treasury_currency_id'
        );
        
        $this->createIndex(
            'idx-acc_cash_order-financial_year_id',
            'acc_cash_order',
            'financial_year_id'
        );

        $this->createIndex(
            'idx-acc_cash_order-created_by',
            'acc_cash_order',
            'created_by'
        );

        $this->createIndex(
            'idx-acc_cash_order-cash_request_id',
            'acc_cash_order',
            'cash_request_id'
        );

        $this->createTable('{{%acc_cash_receipt}}', [
            'id' => $this->primaryKey(),
            'tree_item_currency_id' => $this->integer()->notNull(),
            'treasury_currency_id' => $this->integer()->notNull(),
            'financial_year_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'value' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'notes' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-acc_cash_receipt-tree_item_currency_id',
            'acc_cash_receipt',
            'tree_item_currency_id'
        );

        $this->createIndex(
            'idx-acc_cash_receipt-treasury_currency_id',
            'acc_cash_receipt',
            'treasury_currency_id'
        );
        
        $this->createIndex(
            'idx-acc_cash_receipt-financial_year_id',
            'acc_cash_receipt',
            'financial_year_id'
        );

        $this->createIndex(
            'idx-acc_cash_receipt-created_by',
            'acc_cash_receipt',
            'created_by'
        );

        $this->createTable('{{%acc_cash_request}}', [
            'id' => $this->primaryKey(),
            'tree_item_currency_id' => $this->integer()->notNull(),
            'treasury_currency_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'value' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'notes' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-acc_cash_request-tree_item_currency_id',
            'acc_cash_request',
            'tree_item_currency_id'
        );

        $this->createIndex(
            'idx-acc_cash_request-treasury_currency_id',
            'acc_cash_request',
            'treasury_currency_id'
        );
        
        $this->createIndex(
            'idx-acc_cash_request-created_by',
            'acc_cash_request',
            'created_by'
        );

        $this->createTable('{{%acc_financial_year}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
            'notes' => $this->text(),
            'is_active' => 'tinyint(1) NOT NULL DEFAULT 0',          
            'created_by' => $this->integer()->notNull(),
            'closed_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'closed_at' => $this->timestamp()->null(),
        ], $tableOptions);

        $this->createIndex(
            'idx-acc_financial_year-created_by',
            'acc_financial_year',
            'created_by'
        );


        $this->createIndex(
            'idx-acc_financial_year-closed_by',
            'acc_financial_year',
            'closed_by'
        );

        $this->createTable('{{%acc_financial_year_record}}', [
            'id' => $this->primaryKey(),
            'tree_item_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'financial_year_id' => $this->integer()->notNull(),
            'budget' => $this->double()->notNull()->defaultValue(0),
            'jan_total_cash' => $this->double()->notNull(),
            'jan_total_cheques' => $this->double()->notNull(),
            'feb_total_cash' => $this->double()->notNull(),
            'feb_total_cheques' => $this->double()->notNull(),
            'mar_total_cash' => $this->double()->notNull(),
            'mar_total_cheques' => $this->double()->notNull(),
            'apr_total_cash' => $this->double()->notNull(),
            'apr_total_cheques' => $this->double()->notNull(),
            'may_total_cash' => $this->double()->notNull(),
            'may_total_cheques' => $this->double()->notNull(),
            'jun_total_cash' => $this->double()->notNull(),
            'jun_total_cheques' => $this->double()->notNull(),
            'jul_total_cash' => $this->double()->notNull(),
            'jul_total_cheques' => $this->double()->notNull(),
            'aug_total_cash' => $this->double()->notNull(),
            'aug_total_cheques' => $this->double()->notNull(),
            'sep_total_cash' => $this->double()->notNull(),
            'sep_total_cheques' => $this->double()->notNull(),
            'oct_total_cash' => $this->double()->notNull(),      
            'oct_total_cheques' => $this->double()->notNull(),
            'nov_total_cash' => $this->double()->notNull(),
            'nov_total_cheques' => $this->double()->notNull(),
            'dec_total_cash' => $this->double()->notNull(),
            'dec_total_cheques' => $this->double()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),

        ], $tableOptions);

        $this->createIndex(
            'idx-acc_financial_year_record-tree_item_id',
            'acc_financial_year_record',
            'tree_item_id'
        );

        $this->createIndex(
            'idx-acc_financial_year_record-currency_id',
            'acc_financial_year_record',
            'currency_id'
        );


        $this->createIndex(
            'idx-acc_financial_year_record-financial_year_id',
            'acc_financial_year_record',
            'financial_year_id'
        );

        $this->createTable('{{%acc_tree_item}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'type' => 'tinyint(1) NOT NULL DEFAULT 0',          
            'budget' => $this->double()->notNull()->defaultValue(0),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-acc_tree_item-parent_id',
            'acc_tree_item',
            'parent_id'
        );

        $this->createTable('{{%acc_tree_item_currency}}', [
            'id' => $this->primaryKey(),
            'tree_item_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'jan_total_cash' => $this->double()->notNull()->defaultValue(0),
            'jan_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'feb_total_cash' => $this->double()->notNull()->defaultValue(0),
            'feb_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'mar_total_cash' => $this->double()->notNull()->defaultValue(0),
            'mar_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'apr_total_cash' => $this->double()->notNull()->defaultValue(0),
            'apr_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'may_total_cash' => $this->double()->notNull()->defaultValue(0),
            'may_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'jun_total_cash' => $this->double()->notNull()->defaultValue(0),
            'jun_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'jul_total_cash' => $this->double()->notNull()->defaultValue(0),
            'jul_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'aug_total_cash' => $this->double()->notNull()->defaultValue(0),
            'aug_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'sep_total_cash' => $this->double()->notNull()->defaultValue(0),
            'sep_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'oct_total_cash' => $this->double()->notNull()->defaultValue(0),      
            'oct_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'nov_total_cash' => $this->double()->notNull()->defaultValue(0),
            'nov_total_cheques' => $this->double()->notNull()->defaultValue(0),
            'dec_total_cash' => $this->double()->notNull()->defaultValue(0),
            'dec_total_cheques' => $this->double()->notNull()->defaultValue(0),

        ], $tableOptions);

        $this->createIndex(
            'idx-acc_tree_item_currency-tree_item_id',
            'acc_tree_item_currency',
            'tree_item_id'
        );

        $this->createIndex(
            'idx-acc_tree_item_currency-currency_id',
            'acc_tree_item_currency',
            'currency_id'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
