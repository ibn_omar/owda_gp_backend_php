<?php

use yii\db\Migration;

class m170410_153329_services_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'price' => $this->double()->notNull()->defaultValue(0),
            'service_time' => $this->integer()->notNull()->defaultValue(0),
            'details' => $this->text()->notNull(),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0',
        ], $tableOptions);

        $this->createIndex(
            'idx-service-category_id',
            'service',
            'category_id'
        );

        $this->addForeignKey(
            'fk-service-category_id',
            'service',
            'category_id',
            'service_category',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
