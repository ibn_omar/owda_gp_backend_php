<?php

use yii\db\Migration;

class m170410_173809_acc_foriegn_keys extends Migration
{
    public function up()
    {
        // ==============================================
        // ========  Cash Deposit Foreign Keys ==========
        // ==============================================
        $this->addForeignKey(
            'fk-acc_cash_deposit-financial_year_id',
            'acc_cash_deposit',
            'financial_year_id',
            'acc_financial_year',
            'id',
            'RESTRICT'
        );
                    
        $this->addForeignKey(
            'fk-acc_cash_deposit-tree_item_currency_id',
            'acc_cash_deposit',
            'tree_item_currency_id',
            'acc_tree_item_currency',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-acc_cash_deposit-treasury_currency_id',
            'acc_cash_deposit',
            'treasury_currency_id',
            'treasury_currency',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-acc_cash_deposit-created_by',
            'acc_cash_deposit',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );

        // ==============================================
        // ========  Cash order Foreign Keys ============
        // ==============================================
        $this->addForeignKey(
            'fk-acc_cash_order-financial_year_id',
            'acc_cash_order',
            'financial_year_id',
            'acc_financial_year',
            'id',
            'RESTRICT'
        );
                    
        $this->addForeignKey(
            'fk-acc_cash_order-tree_item_currency_id',
            'acc_cash_order',
            'tree_item_currency_id',
            'acc_tree_item_currency',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-acc_cash_order-treasury_currency_id',
            'acc_cash_order',
            'treasury_currency_id',
            'treasury_currency',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-acc_cash_order-cash_request_id',
            'acc_cash_order',
            'cash_request_id',
            'acc_cash_request',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-acc_cash_order-created_by',
            'acc_cash_order',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );
       
        // ==============================================
        // ========  Cash Receipt Foreign Keys ==========
        // ==============================================
        $this->addForeignKey(
            'fk-acc_cash_receipt-financial_year_id',
            'acc_cash_receipt',
            'financial_year_id',
            'acc_financial_year',
            'id',
            'RESTRICT'
        );
                    
        $this->addForeignKey(
            'fk-acc_cash_receipt-tree_item_currency_id',
            'acc_cash_receipt',
            'tree_item_currency_id',
            'acc_tree_item_currency',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-acc_cash_receipt-treasury_currency_id',
            'acc_cash_receipt',
            'treasury_currency_id',
            'treasury_currency',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-acc_cash_receipt-created_by',
            'acc_cash_receipt',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );
        
        // ==============================================
        // ========  Cash Request Foreign Keys ==========
        // ==============================================

        $this->addForeignKey(
            'fk-acc_cash_request-tree_item_currency_id',
            'acc_cash_request',
            'tree_item_currency_id',
            'acc_tree_item_currency',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-acc_cash_request-treasury_currency_id',
            'acc_cash_request',
            'treasury_currency_id',
            'treasury_currency',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-acc_cash_request-created_by',
            'acc_cash_request',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );
        
        // ================================================
        // ========  Financial Year Foreign Keys ==========
        // ================================================

        $this->addForeignKey(
            'fk-acc_financial_year-closed_by',
            'acc_financial_year',
            'closed_by',
            'user',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-acc_financial_year-created_by',
            'acc_financial_year',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );


        // =======================================================
        // ========  Financial Year Record Foreign Keys ==========
        // =======================================================
        
        $this->addForeignKey(
            'fk-acc_financial_year_record-financial_year_id',
            'acc_financial_year_record',
            'financial_year_id',
            'acc_financial_year',
            'id',
            'RESTRICT'
        );
                    
        $this->addForeignKey(
            'fk-acc_financial_year_record-tree_item_id',
            'acc_financial_year_record',
            'tree_item_id',
            'acc_tree_item',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-acc_financial_year_record-currency_id',
            'acc_financial_year_record',
            'currency_id',
            'currency',
            'id',
            'RESTRICT'
        );


        // ===========================================
        // ========  Tree Item Foreign Keys ==========
        // ===========================================

        $this->addForeignKey(
            'fk-acc_tree_item-parent_id',
            'acc_tree_item',
            'parent_id',
            'acc_tree_item',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-acc_tree_item-created_by',
            'acc_tree_item',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );



        // =======================================================
        // ========  Tree Item Currency Foreign Keys =============
        // =======================================================
                    
        $this->addForeignKey(
            'fk-acc_tree_item_currency-tree_item_id',
            'acc_tree_item_currency',
            'tree_item_id',
            'acc_tree_item',
            'id',
            'RESTRICT'
        );
                        
        $this->addForeignKey(
            'fk-acc_tree_item_currency-currency_id',
            'acc_tree_item_currency',
            'currency_id',
            'currency',
            'id',
            'RESTRICT'
        );

        // =====================================
        // ========  treasury Foreign Keys ======
        // =====================================

        $this->addForeignKey(
            'fk-treasury-created_by',
            'treasury',
            'created_by',
            'user',
            'id',
            'RESTRICT'
        );

        // =====================================
        // ========  treasury_currency Foreign Keys ======
        // =====================================

        $this->addForeignKey(
            'fk-treasury_currency-treasury_id',
            'treasury_currency',
            'treasury_id',
            'treasury',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-treasury_currency-currency_id',
            'treasury_currency',
            'currency_id',
            'currency',
            'id',
            'RESTRICT'
        );


    }

    public function down()
    {
        echo "m170410_173809_acc_foriegn_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
