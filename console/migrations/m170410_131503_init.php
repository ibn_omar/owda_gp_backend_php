<?php

use yii\db\Migration;

class m170410_131503_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->null(),
            'role_id' => $this->integer()->notNull(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createTable('{{%brand}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string(),
            'details' => $this->text(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0'
        ], $tableOptions);

        $this->createTable('{{%pos_product_category}}', [
            'id' => $this->primaryKey(),
            'parent_category_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'code' => $this->string(),
            'details' => $this->text(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0'
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_product_category-parent_category_id',
            'pos_product_category',
            'parent_category_id'
        );

        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'unit_id' => $this->integer()->notNull(),
            'brand_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'code' => $this->string(),
            'bar_code' => $this->string(),
            'cost' => $this->double()->notNull()->defaultValue(0),
            'retail_price' => $this->double()->notNull()->defaultValue(0),
            'wholesale_price' => $this->double()->notNull()->defaultValue(0),
            'min_quantity' => $this->integer()->defaultValue(0),
            'has_discount' => 'tinyint(1) NOT NULL DEFAULT 0',
            'discount_type' => 'tinyint(1)',
            'discount_value' => $this->double()->defaultValue(0),
            'has_taxes' => 'tinyint(1) NOT NULL DEFAULT 0',
            'taxes_type' => 'tinyint(1)',
            'taxes_value' => $this->double()->defaultValue(0),
            'details' => $this->text(),
            'image' => $this->text(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer()->notNull(),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0'
        ], $tableOptions);


        $this->createIndex(
            'idx-product-unit_id',
            'product',
            'unit_id'
        );

        $this->createIndex(
            'idx-product-brand_id',
            'product',
            'brand_id'
        );

        $this->createIndex(
            'idx-product-category_id',
            'product',
            'category_id'
        );

        $this->createIndex(
            'idx-product-created_by',
            'product',
            'created_by'
        );

        $this->createTable('{{%pos_store}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string(),
            'details' => $this->text(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0'
        ], $tableOptions);

        $this->createTable('{{%pos_offer}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'is_active' => 'tinyint(1) NOT NULL DEFAULT 0',
            'max_quantity' => $this->integer(),
            'type' => 'tinyint(1) NOT NULL DEFAULT 0',
            'value' => $this->double()->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ], $tableOptions);


        $this->createIndex(
            'idx-pos_offer-product_id',
            'pos_offer',
            'product_id'
        );

        $this->createIndex(
            'idx-pos_offer-created_by',
            'pos_offer',
            'created_by'
        );

        $this->createTable('{{%pos_offer_store}}', [
            'id' => $this->primaryKey(),
            'offer_id' => $this->integer()->notNull(),
            'store_id' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex(
            'idx-pos_offer_store-offer_id',
            'pos_offer_store',
            'offer_id'
        );

        $this->createIndex(
            'idx-pos_offer_store-store_id',
            'pos_offer_store',
            'store_id'
        );

        $this->createTable('{{%pos_product_price_change}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'price_type' => 'tinyint(1) NOT NULL',
            'old_price' => $this->double()->notNull(),
            'new_price' => $this->double()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ], $tableOptions);


        $this->createIndex(
            'idx-pos_product_price_change-product_id',
            'pos_product_price_change',
            'product_id'
        );

        $this->createIndex(
            'idx-pos_product_price_change-created_by',
            'pos_product_price_change',
            'created_by'
        );

        $this->createTable('{{%pos_order}}', [
            'id' => $this->primaryKey(),
            'financial_year_id' => $this->integer()->notNull(),
            'treasury_currency_id' => $this->integer()->notNull(),
            'tree_item_currency_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'store_id' => $this->integer()->notNull(),
            'code' => $this->string(),
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'extra_price' => $this->double()->notNull()->defaultValue(0),
            'total_price' => $this->double()->notNull(),
            'discount' => $this->double()->notNull()->defaultValue(0),
            'total_refund' => $this->double()->notNull()->defaultValue(0),
            'notes' => $this->text(),
            'refund_notes' => $this->text(),
            'is_refunded' => 'tinyint(1) NOT NULL DEFAULT 0',
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'refunded_at' => $this->timestamp()->null(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ], $tableOptions);


        $this->createIndex(
            'idx-pos_order-created_by',
            'pos_order',
            'created_by'
        );

        $this->createIndex(
            'idx-pos_order-client_id',
            'pos_order',
            'client_id'
        );

        $this->createTable('{{%pos_order_item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->double()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_order_item-order_id',
            'pos_order_item',
            'order_id'
        );

        $this->createIndex(
            'idx-pos_order_item-product_id',
            'pos_order_item',
            'product_id'
        );

        $this->createTable('{{%pos_store_product}}', [
            'id' => $this->primaryKey(),
            'store_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull()->defaultValue(0),
            'reserved_quantity' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_store_product-store_id',
            'pos_store_product',
            'store_id'
        );

        $this->createIndex(
            'idx-pos_store_product-product_id',
            'pos_store_product',
            'product_id'
        );

        $this->createTable('{{%pos_supplier}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'mobile' => $this->string(15)->notNull(),
            'email' => $this->string()->notNull(),
            'region_id' => $this->integer()->notNull(),
            'address' => $this->text(),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0',
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_supplier-region_id',
            'pos_supplier',
            'region_id'
        );

        $this->createTable('{{%pos_supplier_treasury_paper}}', [
            'id' => $this->primaryKey(),
            'financial_year_id' => $this->integer()->notNull(),
            'treasury_currency_id' => $this->integer()->notNull(),
            'tree_item_currency_id' => $this->integer()->notNull(),
            'supplier_id' => $this->integer()->notNull(),
            'type' => 'tinyint(1) NOT NULL',
            'purpose' => $this->string(),
            'value' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'notes' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_supplier_treasury_paper-financial_year_id',
            'pos_supplier_treasury_paper',
            'financial_year_id'
        );
        
        $this->createIndex(
            'idx-pos_supplier_treasury_paper-treasury_currency_id',
            'pos_supplier_treasury_paper',
            'treasury_currency_id'
        );
        
        $this->createIndex(
            'idx-pos_supplier_treasury_paper-tree_item_currency_id',
            'pos_supplier_treasury_paper',
            'tree_item_currency_id'
        );
        
        $this->createIndex(
            'idx-pos_supplier_treasury_paper-supplier_id',
            'pos_supplier_treasury_paper',
            'supplier_id'
        );

        $this->createIndex(
            'idx-pos_supplier_treasury_paper-created_by',
            'pos_supplier_treasury_paper',
            'created_by'
        );

        $this->createTable('{{%pos_supply_order}}', [
            'id' => $this->primaryKey(),
            'supplier_id' => $this->integer()->notNull(),
            'store_id' => $this->integer()->notNull(),
            'code' => $this->string(),
            'type' => 'tinyint(1) NOT NULL',
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'extra_price' => $this->double()->notNull(),
            'total_price' => $this->double()->notNull(),
            'discount' => $this->double()->notNull(),
            'notes' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_supply_order-supplier_id',
            'pos_supply_order',
            'supplier_id'
        );

        $this->createIndex(
            'idx-pos_supply_order-store_id',
            'pos_supply_order',
            'store_id'
        );

        $this->createIndex(
            'idx-pos_supply_order-created_by',
            'pos_supply_order',
            'created_by'
        );

        $this->createTable('{{%pos_supply_order_item}}', [
            'id' => $this->primaryKey(),
            'supply_order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->double()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_supply_order_item-supply_order_id',
            'pos_supply_order_item',
            'supply_order_id'
        );

        $this->createIndex(
            'idx-pos_supply_order_item-product_id',
            'pos_supply_order_item',
            'product_id'
        );


        $this->createTable('{{%pos_transfer_item}}', [
            'id' => $this->primaryKey(),
            'transfer_order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'unit_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_transfer_item-transfer_order_id',
            'pos_transfer_item',
            'transfer_order_id'
        );

        $this->createIndex(
            'idx-pos_transfer_item-product_id',
            'pos_transfer_item',
            'product_id'
        );

        $this->createIndex(
            'idx-pos_transfer_item-unit_id',
            'pos_transfer_item',
            'unit_id'
        );

        $this->createTable('{{%pos_transfer_order}}', [
            'id' => $this->primaryKey(),
            'store_from_id' => $this->integer()->notNull(),
            'store_to_id' => $this->integer()->notNull(),
            'code' => $this->string(),
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'notes' => $this->text(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ], $tableOptions);

        $this->createIndex(
            'idx-pos_transfer_order-store_from_id',
            'pos_transfer_order',
            'store_from_id'
        );

        $this->createIndex(
            'idx-pos_transfer_order-store_to_id',
            'pos_transfer_order',
            'store_to_id'
        );

        $this->createIndex(
            'idx-pos_transfer_order-created_by',
            'pos_transfer_order',
            'created_by'
        );

        $this->createTable('{{%unit}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'sign' => $this->string()->notNull(),
            'details' => $this->text(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0',
        ], $tableOptions);

        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'is_active' => 'tinyint(1) NOT NULL DEFAULT 0',
            'description' => $this->text(),
        ], $tableOptions);

        $this->createTable('{{%view}}', [
            'id' => $this->primaryKey(),
            'module_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'is_ajax' => 'tinyint(1) NOT NULL DEFAULT 0',
            'description' => $this->text(),
        ], $tableOptions);

        $this->createIndex(
            'idx-view-module_id',
            'view',
            'module_id'
        );

        $this->createTable('{{%role_view}}', [
            'id' => $this->primaryKey(),
            'role_id' => $this->integer(),
            'view_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-role_view-role_id',
            'role_view',
            'role_id'
        );

        $this->createIndex(
            'idx-role_view-view_id',
            'role_view',
            'view_id'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
