<?php

use yii\db\Migration;

class m170410_161534_car_checkin extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%car_checkin}}', [
            'id' => $this->primaryKey(),
            'car_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'checkin' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-car_checkin-car_id',
            'car_checkin',
            'car_id'
        );

        $this->addForeignKey(
            'fk-car_checkin-car_id',
            'car_checkin',
            'car_id',
            'car',
            'id',
            'RESTRICT'
        );

    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
