<?php

use yii\db\Migration;

class m170410_155121_blocks_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%block}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'max_capacity' => $this->integer()->notNull(),
            'is_archived' => 'tinyint(1) NOT NULL DEFAULT 0',
        ], $tableOptions);
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
