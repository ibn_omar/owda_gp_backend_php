<?php

use yii\db\Migration;

class m170410_163224_car_checkin_details extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%car_checkin_details}}', [
            'id' => $this->primaryKey(),
            'car_checkin_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
            'start_service' => $this->integer()->defaultValue(0),
            'estimation_time' => $this->integer()->notNull(),
            'discount' => $this->double()->defaultValue(0),
            'extra_cost' => $this->double()->defaultValue(0),
            'rate' => $this->integer(),
            'is_called' => $this->integer()->defaultValue(0),
            'comment' => $this->text(),
        ], $tableOptions);

        $this->createIndex(
            'idx-car_checkin_details-car_checkin_id',
            'car_checkin_details',
            'car_checkin_id'
        );

        $this->createIndex(
            'idx-car_checkin_details-service_id',
            'car_checkin_details',
            'service_id'
        );

        $this->addForeignKey(
            'fk-car_checkin_details-car_checkin_id',
            'car_checkin_details',
            'car_checkin_id',
            'car_checkin',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-car_checkin_details-service_id',
            'car_checkin_details',
            'service_id',
            'service',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
