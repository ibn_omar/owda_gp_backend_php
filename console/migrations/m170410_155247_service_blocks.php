<?php

use yii\db\Migration;

class m170410_155247_service_blocks extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%service_block}}', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer()->notNull(),
            'block_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-service_block-service_id',
            'service_block',
            'service_id'
        );

        $this->createIndex(
            'idx-service_block-block_id',
            'service_block',
            'block_id'
        );

        $this->addForeignKey(
            'fk-service_block-service_id',
            'service_block',
            'service_id',
            'service',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-service_block-block_id',
            'service_block',
            'block_id',
            'block',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
