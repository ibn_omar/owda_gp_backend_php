<?php

use yii\db\Migration;

class m170410_154858_service_product_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%service_product}}', [
            'id' => $this->primaryKey(),
            'service_id' => $this->integer()->notNull(),
            'store_product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-service_product-service_id',
            'service_product',
            'service_id'
        );

        $this->createIndex(
            'idx-service_product-store_product_id',
            'service_product',
            'store_product_id'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
