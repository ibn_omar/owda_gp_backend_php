<?php

use yii\db\Migration;

class m170410_160506_car_manufacturer_category extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%car_manufacturer_category}}', [
            'id' => $this->primaryKey(),
            'car_manufacturer_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-car_manufacturer_category-car_manufacturer_id',
            'car_manufacturer_category',
            'car_manufacturer_id'
        );

        $this->addForeignKey(
            'fk-car_manufacturer_category-car_manufacturer_id',
            'car_manufacturer_category',
            'car_manufacturer_id',
            'car_manufacturer',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
