<?php

use yii\db\Migration;

class m170410_152503_extended_accounting_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%treasury}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'is_active' => 'tinyint(1) NOT NULL DEFAULT 1',
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);
        
        $this->createIndex(
            'idx-treasury-created_by',
            'treasury',
            'created_by'
        );

        $this->createTable('{{%treasury_currency}}', [
            'id' => $this->primaryKey(),
            'treasury_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'cash' => $this->double()->notNull()->defaultValue(0),
            'cheques' => $this->double()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-treasury_currency-treasury_id',
            'treasury_currency',
            'treasury_id'
        );
        
        $this->createIndex(
            'idx-treasury_currency-currency_id',
            'treasury_currency',
            'currency_id'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
