<?php

use yii\db\Migration;

class m170410_162149_car_checkout extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%car_checkout}}', [
            'id' => $this->primaryKey(),
            'car_checkin_id' => $this->integer()->notNull(),
            'estimated_date' => $this->timestamp()->notNull(),
            'actual_date' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex(
            'idx-car_checkout-car_checkin_id',
            'car_checkout',
            'car_checkin_id'
        );

        $this->addForeignKey(
            'fk-car_checkout-car_checkin_id',
            'car_checkout',
            'car_checkin_id',
            'car_checkin',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
