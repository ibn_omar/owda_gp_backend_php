<?php

use yii\db\Migration;

class m170410_160634_client extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(),
            'region_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'address' => $this->text()->notNull(),
            'phone' => $this->string()->notNull(),
            'email' => $this->string(),
        ], $tableOptions);

        $this->createIndex(
            'idx-client-city_id',
            'client',
            'city_id'
        );

        $this->createIndex(
            'idx-client-region_id',
            'client',
            'region_id'
        );

        $this->addForeignKey(
            'fk-client-city_id',
            'client',
            'city_id',
            'city',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-client-region_id',
            'client',
            'region_id',
            'region',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
