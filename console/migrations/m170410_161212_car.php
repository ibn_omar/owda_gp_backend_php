<?php

use yii\db\Migration;

class m170410_161212_car extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%car}}', [
            'id' => $this->primaryKey(),
            'car_manufacturer_category_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'plate_number' => $this->integer()->notNull(),
            'color' => $this->string(),
        ], $tableOptions);

        $this->createIndex(
            'idx-car-car_manufacturer_category_id',
            'car',
            'car_manufacturer_category_id'
        );

        $this->createIndex(
            'idx-car-client_id',
            'car',
            'client_id'
        );

        $this->addForeignKey(
            'fk-car-car_manufacturer_category_id',
            'car',
            'car_manufacturer_category_id',
            'car_manufacturer_category',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-car-client_id',
            'car',
            'client_id',
            'client',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
