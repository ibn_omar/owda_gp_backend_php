<?php
namespace common\helpers;

use Yii;
use common\models\User;
use common\models\Module;
use admin\models\SystemPreferences;

class UserHelper{
	public function can($moduleName,$privelige){
        $module_id = Module::find()->where(['url'=>$moduleName.'/'])->one()->id;
        $res = SystemPreferences::find()->where(['module_id'=>$module_id])->one();

        if ($res->can_access == 1){
            $user = User::find()->where(['id'=>Yii::$app->session->get('userid')])->one();
            $privelige_active = $user->role->is_active;

            if ($privelige_active == 1){
                $role_id = $user->role_id;
                $command = Yii::$app->db->createCommand("SELECT COUNT(view.id) as count FROM view JOIN module ON view.module_id = module.id WHERE module.url = '".$moduleName."/' AND view.url = '".$privelige."' AND (view.is_ajax = 1 OR view.id in (SELECT view_id FROM role_view WHERE role_view.role_id = ".$role_id."))");
                $result = $command->queryAll();

                if ($result[0]['count'] > 0){
                    return true;
                }

                else{
                    //It must be return false :(
                    //But Not all views has been imported to the database
                    //Until then, Deal with it :D :D
                    return false;
                }
            }

            else{
                return true;
            }
        }

        else{
            return false;
        }
	}
}
?>