<?php

namespace common\helpers;

use Yii;

class ModelHelper extends \yii\db\ActiveRecord
{

	public function beforeValidate()
    {
    	if (parent::beforeValidate()) {
            if ($this->isNewRecord) {
                //$this->created_by = Yii::$app->session->get('userid');
                $this->created_by = 1;

            }
    		return true;
    	}

    	return false;
    }
}