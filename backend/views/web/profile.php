<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>


<div class="">
    	<div class="col-md-8">
            <label>  Name: 
				<?= $user->fname . " " . $user->lname ?>
			</label>
   		</div>

   		<div class="col-md-4">
            <label>  User Name : 
				<?= $user->username ?>
			</label>
   		</div>
</div>

<div class="">

   		<div class="col-md-6">
            <label>  E-mail: 
				<?= $user->email ?>
			</label>
   		</div>

   		<div class="col-md-6">
            <label>  Phone: 
				<?= $user->phone ?>
			</label>
   		</div>
</div>

<div class="">
    	<div class="col-md-4">
            <label>  Age : 
				<?= $user->age ?>
			</label>
   		</div>


   		<div class="col-md-4">
            <label>  Citizen of : 
				<?= $user->nationality->name ?>
			</label>
   		</div>

      <div class="col-md-4">
            <label>  Educational Level : 
        <?= $user->edulevel->level_name ?>
      </label>
      </div>

   		<div class="col-md-4">
            <label>  Rate : 
				<?= $user->rate ?>
			</label>
   		</div>
</div>


<div class="">
    	<div class="col-md-6">
            <label>  Marital Status : 
				<?php 
				$status= $user->marital_status ; 
				if ($status == 0) echo "Single";
				else if ($status == 1) echo "Engaged" ;
				else if ($status == 2) echo "Married" ;
				else if ($status == 3) echo "Divorced" ;
				else if ($status == 4) echo "Widowed" ;


				?>
			</label>
   		</div>


   		<div class="col-md-6">
            <label>  Smoker ? : 
				<?php 
				$is_smoker =$user->is_smoker ;
				if ($is_smoker == 1) echo "Yes";
				else echo "No" ;

				?>
			</label>
   		</div>
</div>

<a class="btn btn-xs btn-default" href="<?=Url::to(['web/edit'])?>"><span class="icon-pencil">Change Info</span></a> 


