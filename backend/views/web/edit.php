<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js">
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('select').selectize();  

    /* $("#signup_form").validate({     
            rules: {
                'email':'required',
                'password':'required',
            }
        });*/
});

</script>

<?php if(Yii::$app->session->hasFlash('error')):?>
    <div class="alert alert-danger">
        <?php echo Yii::$app->session->getFlash('error') ?>
    
    </div>
<?php endif; ?>

<div id="rendering-context">
        <div class="portlet-body form">
        	<form role="form" id="signup_form" action="<?= Url::to(['web/edit']) ?>" method="post" enctype="multipart/form-data">
        		<div class="row">
        			<div class="form-group col-md-6">
                            <label>First Name</label>
                            <input type="text" name="User[fname]" placeholder="Eg. Mohammed" class="form-control" value="<?= $user->fname ?>" >
                	</div>

                	<div class="form-group col-md-6">
                            <label>Last Name</label>
                            <input type="text" name="User[lname]" placeholder="Eg. Ahmed" class="form-control" value="<?= $user->lname ?>" >
                	</div>

                </div>
                <div class="row">
                	<div class="form-group col-md-3">
                            <label>Email</label>
                            <input type="email" name="User[email]" placeholder="Mohammedahmed1993@xyz.com" class="form-control" value="<?= $user->email ?>">
                	</div>


                	<div class="form-group col-md-3">
                            <label>Phone Number</label>
                            <input type="text" name="User[phone]" placeholder="+201112232123" class="form-control" value="<?= $user->phone ?>">
                	</div>

                	<div class="form-group col-md-3">
                            <label>User Name</label>
                            <input type="text" name="User[username]" placeholder="Mohammedahmed1993" class="form-control" value="<?= $user->username ?>">
                	</div>

                	<div class="form-group col-md-3">
                            <label>Password</label>
                            <input type="password" name="User[password]" placeholder="Mohammedahmed1993@xyz.com" class="form-control" value="<?= $user->password ?>">
                	</div>

                </div>

                <div class="row">

                	<div class="form-group col-md-12 error-container">
                            <label>Nationality</label>
                            <select name="User[Nationality]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <?php
                                    foreach ($nationalities as $nationality){
                                ?>
                                <option value="<?= $nationality["id"];?>"
                                 <?php if($user->Nationality == $nationality["id"]){echo 'selected';}?>
                                ><?= $nationality["name"] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                    </div>

                     <div class="form-group col-md-12 error-container">
                            <label>Educational Level</label>
                            <select name="User[educational_level]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <?php
                                    foreach ($education_level as $education_level){
                                ?>
                                <option value="<?= $education_level["id"];?>"
                                    <?php if($user->educational_level == $education_level["id"]){echo 'selected';}?>
                                ><?= $education_level["level_name"] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                    </div>

                </div>

                <div class="row">
                	<div class="form-group col-md-4">
                            <label>Age</label>
                            <input type="text" name="User[age]" placeholder="23" class="form-control" value="<?= $user->age ?>">
                	</div>


                	<div class="form-group col-md-4">
                            <label>Marital Status</label>
                            <select name="User[marital_status]" class="bs-select form-control add-select" >
                            	<option value=""></option>
                                <option value="0" <?php if($user->marital_status == 0){echo 'selected';}?>>Single</option>
                                <option value="1" <?php if($user->marital_status == 1){echo 'selected';}?> >Engaged</option>
                                <option value="2" <?php if($user->marital_status == 2){echo 'selected';}?>>Married</option>
                                <option value="3" <?php if($user->marital_status == 3){echo 'selected';}?>>Divorced</option>
                                <option value="4" <?php if($user->marital_status == 4){echo 'selected';}?>>widowed</option>
                                
                            </select>
                	</div>

                	<div class="form-group col-md-4">
                            <label>Are You a Smoker ?</label>
                            <select name="User[is_smoker]" class="bs-select form-control add-select" >
                            	<option value=""></option>
                                <option value="0" <?php if($user->is_smoker == 0){echo 'selected';}?>>Yes</option>
                                <option value="1" <?php if($user->is_smoker == 1){echo 'selected';}?>>No</option>
                            </select>
                	</div>

                </div>

                <div class="row">
                	<div class="form-group col-md-3">
                            <button id="signin" type="submit" class="btn blue pull-right">Update Infos</button>
               		</div>

               	</div>
        	</form>

        </div>

</div>