<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<script>
    $( document ).ready(function() {
        $('#previewTable').dataTable();       
   });
</script>  


<div class="portlet-body form">

	<div class="portlet-body">
	    <table class="table table-striped table-hover dt-responsive" width="100%" id="previewTable">
	        <thead>
	            <tr>
	                <th></th>
	                <th class="all">Title</th>
	                <th class="none">Location</th>
	                <th class="all">Price</th>
	                <th class="none">InAdvance Payment</th>
	                <th class="none">Include Maintenance</th>
	                <th class="all">View Add</th>
	            </tr>
	        </thead>
	        <tbody>
	        <?php foreach ($my_adds as $advertisement): ?>
	            <tr>
	                <th></th>
	                <td><?= $advertisement->title ?></td>
	                <td><?= $advertisement->room->apartment->address ?></td>
	                <td><?= $advertisement->price ?></td>
	                <td><?php
	                 	if($advertisement->inadvance_pay == 0) echo "No";
	                 	else if($advertisement->inadvance_pay == 1) echo "Yes";
	                  ?></td>
	                <td><?php 
	                	if($advertisement->including_maintenance == 0) echo "No";
	                 	else if($advertisement->including_maintenance == 1) echo "Yes";
	                ?></td>
	                <td>
	                    <a href="#" class="btn btn-default btn-xs"><span class="fa-eye" aria-hidden="true">View</span></a>
	                </td>
	            </tr>
	        <?php endforeach; ?>       
	        </tbody>
	    </table>
    </div>


</div>
