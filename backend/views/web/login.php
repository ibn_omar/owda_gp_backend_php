<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js">
</script>
<script type="text/javascript">
    $(document).ready(function(){
     $("#login_form").validate({     
            rules: {
                'email':'required',
                'password':'required',
            }
        });
});

</script>

<style type="text/css">
    
    #signup{ 
    position: absolute;
    top:40%;
    left:50%;
    padding:15px;
    : translateX(-50%)
    translateY(-50%);
    transform: translate(-50%,-50%);

    }
</style>

<?php if(Yii::$app->session->hasFlash('error')):?>
    <div class="alert alert-danger">
        <?php echo Yii::$app->session->getFlash('error') ?>
    
    </div>
<?php endif; ?>

<div id="rendering-context">
        <div class="portlet-body form">
        	<form role="form" id="login_form" action="<?= Url::to(['web/login']) ?>" method="post" enctype="multipart/form-data">
        		<div class="row">
        			<div class="form-group col-md-6">
                            <label>User Email</label>
                            <input type="email" name="email" placeholder="Your Email" class="form-control">
                	</div>

                </div>
                <div class="row">
                	<div class="form-group col-md-6">
                            <label>User Password</label>
                            <input type="password" name="password" placeholder="Your Password" class="form-control">
                	</div>
                </div>

                <div class="row">
                	<div class="form-group col-md-3">
                            <button id="signin" type="submit" class="btn blue pull-right">LogIn</button>
               		</div>

               	</div>
        	</form>

        </div>

        <div >
            Don't Have an Account ?
            <a href="<?=Url::to(['web/signup'])?>" ><span>Sign Up</span></a> <br>
            By Logging in You agree to our 
            <a href="<?=Url::to(['web/legal-disclaimer'])?>" ><span>Terms of use</span></a>
        </div>


</div>