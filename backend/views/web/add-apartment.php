<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<script src="">
</script>
<script type="text/javascript">
 /*   $(document).ready(function(){
    $('select').selectize();  
/*
});

</script>
<h3>Add an Apartment</h3><br>
<div id="rendering-context">
        <div class="portlet-body form">
            <form role="form" id="signup_form" action="<?= Url::to(['web/add-apartment']) ?>" method="post" enctype="multipart/form-data">
                
                <div class="row">

                    <div class="form-group col-md-3">
                            <label>Ownership Type</label>
                            <select name="Apartments[ownership_type]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">Rented apartment</option>
                                <option value="1">Owned apartment</option>
                            </select>
                    </div>

                    <div class="form-group col-md-3">
                            <label>City</label>
                            <select name="Apartments[city_id]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <?php
                                    foreach ($cities as $city){
                                ?>
                                <option value="<?= $city["id"];?>"><?= $city["name"] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                    </div>
                   

                    <div class="form-group col-md-3">
                            <label>Address</label>
                            <input type="text" name="Apartments[address]" placeholder="22b Baker Street" class="form-control">
                    </div>


                    <div class="form-group col-md-3">
                            <label>Nearist Metro Station</label>
                            <input type="text" name="Apartments[nearist_metro]" placeholder="Helwan Station" class="form-control">
                    </div>
                   

                </div>

                <div class="row">

                    <div class="form-group col-md-4">
                            <label>Floor Number</label>
                            <input type="Number" name="Apartments[floor_num]" placeholder="2" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                            <label>Number Of Rooms</label>
                            <input type="Number" name="Apartments[num_of_rooms]" placeholder="8" class="form-control">
                    </div>

                    <!--div class="form-group col-md-4">
                            <label>Address</label>
                            <input type="Number" name="Apartments[address]" placeholder="22b Baker Street" class="form-control">
                    </div-->


                </div>

                <div class="row">

                    <div class="form-group col-md-4">
                            <label>Maximum Inhabittants</label>
                            <input type="Number" name="Apartments[max_inhabitants]" placeholder="5" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                            <label>Current Inhabitants</label>
                            <input type="Number" name="Apartments[current_inhabitants]" placeholder="2" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                            <label>Gender Preferences</label>
                            <select name="Apartments[gender_preferences]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">Males Only</option>
                                <option value="1">Females Only</option>
                            </select>
                    </div>

                </div>


                <div class="row">

                     <div class="form-group col-md-3">
                            <label>Does Your apartment has a kitchedn?</label>
                            <select name="Apartments[has_kitchen]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                    </div>

                    <div class="form-group col-md-3">
                            <label>Is there separate bathrooms for each room in the apartment?</label>
                            <select name="Apartments[separate_bathrooms]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                    </div>


                    <div class="form-group col-md-3">
                            <label>Is there an elevator?</label>
                            <select name="Apartments[has_elevator]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                    </div>

                    <div class="form-group col-md-3">
                            <label>Is it okay to have pets in the apartment?</label>
                            <select name="Apartments[pets_allowance]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                    </div>

                </div>
                <div class="row">
                    

                    <div class="form-group col-md-4">
                            <label>Is smoking allowed in the apartment ?</label>
                            <select name="Apartments[smoking_allowance]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                    </div>

                    <div class="form-group col-md-4">
                            <label>Do you have Wifi in the apartment ?</label>
                            <select name="Apartments[has_wifi]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                    </div>


                    <div class="form-group col-md-4">
                            <label>What is the furnature statusin the apartment ?</label>
                            <select name="Apartments[furniture_status]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">Old</option>
                                <option value="1">Modern</option>
                            </select>
                    </div>

                </div>


                <div class="row">
                    <div class="form-group col-md-12">
                            <button id="signin" type="submit" class="btn blue pull-right">Add Avertisment</button>
                    </div>

                </div>
            </form>

        </div>

</div>