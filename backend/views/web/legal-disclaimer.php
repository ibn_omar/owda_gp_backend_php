
<p>

- By Using this application you agree to our terms of use ; This application aims to facilitate the search and selection of rooms to rent, and to guarantee the maximum quality of service we provide we ask our users to rate the rooms they have been renting and also to rate other users (explained on the next point). <br><br>

- User Rating : The rating is done anonymously, every user starts with a rating of 4/4 then every time the user is rated by others we calculate the average and then make it the new rate. This rate only aims to help other users find more suitable users to live with. <br><br>

- By using this Application you agree to be rated by your mates and to rate your mates.<br><br>

- By Reserving a room via credit card you will be charged with 5% from the total renting amount. <br>

</p>