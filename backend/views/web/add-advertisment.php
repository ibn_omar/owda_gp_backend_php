<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js">
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('select').selectize();  

    /* $("#signup_form").validate({     
            rules: {
                'email':'required',
                'password':'required',
            }
        });*/
});

</script>

<?php if(Yii::$app->session->hasFlash('error')):?>
    <div class="alert alert-danger">
        <?php echo Yii::$app->session->getFlash('error') ?>
    
    </div>
<?php endif; ?>

<h3>Add an Advertisment</h3><br>
<div id="rendering-context">
        <div class="portlet-body form">
        	<form role="form" id="signup_form" action="<?= Url::to(['web/add-advertisment']) ?>" method="post" enctype="multipart/form-data">
        		
                <div class="row">

                    <div class="form-group col-md-4">
                            <label>Advertisment's Title</label>
                            <input type="text" name="Advertisement[title]" placeholder="Room at Gotham" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                            <label>Adertisment's Body</label>
                            <textarea rows="4" cols="50" name="Advertisement[body]" class="form-control" placeholder="" ></textarea>
                    </div>

                   

                    <div class="form-group col-md-4">
                            <label>Price</label>
                            <input type="Number" name="Advertisement[price]" placeholder="1500" class="form-control">
                    </div>
                   


                </div>



                <div class="row">

                     <div class="form-group col-md-4">
                            <label>Does the Price include Maintenance?</label>
                            <select name="Advertisemen[including_maintenance]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">Yes</option>
                                <option value="1">No</option>
                            </select>
                    </div>

                    <div class="form-group col-md-4">
                            <label>Do you demand an Inadvance Payment?</label>
                            <select name="Advertisemen[inadvance_pay]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">Yes</option>
                                <option value="1">No</option>
                            </select>
                    </div>


                    <div class="form-group col-md-4">
                            <label>What is the Room Used As</label>
                            <input type="text" name="Room[used_as]" placeholder="Studio / Living" class="form-control">
                    </div>

                </div>
                <div class="row">
                	

                    <div class="form-group col-md-3">
                            <label>Does the Room has a Bathroom</label>
                            <select name="Room[has_bathroom]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">Yes</option>
                                <option value="1">No</option>
                            </select>
                    </div>

                    <div class="form-group col-md-3">
                            <label>Does the Room has a Balacony</label>
                            <select name="Room[has_balacony]" class="bs-select form-control add-select" >
                                <option value=""></option>
                                <option value="0">Yes</option>
                                <option value="1">No</option>
                            </select>
                    </div>

                    <div class="form-group col-md-3">
                            <label>Number Of beds</label>
                            <input type="Number" name="Room[num_of_beds]" min=1 placeholder="3" class="form-control">
                    </div>

                    <div class="form-group col-md-3">
                            <label>Available Beds</label>
                            <input type="Number" name="Room[available_beds]" min=1 placeholder="1" class="form-control">
                    </div>

                </div>




                <div class="row">

                    


                    <div class="form-group col-md-6">
                            <label>Available From</label>
                            <input type="text" name="Room[available_from]" placeholder="2017-02-27" class="form-control">
                    </div>

                    <div class="form-group col-md-6">
                            <label>Available To</label>
                            <input type="text" name="Room[available_to]" placeholder="2018-05-07" class="form-control">
                    </div>

                </div>


                <div class="row">
                	<div class="form-group col-md-12">
                            <button id="signin" type="submit" class="btn blue pull-right">Add Avertisment</button>
               		</div>

               	</div>
        	</form>

        </div>

</div>