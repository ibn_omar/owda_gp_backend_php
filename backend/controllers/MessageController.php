<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\City;
use backend\models\User;
use backend\models\Room;
use backend\models\Message;
use backend\models\RoomUsers;
use yii\base\Exception;

/**
 * Message controller
 */
class MessageController extends Controller
{

    public $enableCsrfValidation = false;

	public function actionIndex()
    {
        echo "Message Controller";
        exit();
    }


    public function actionSendMessage()
    {
    	$reciever_username = $_POST["reciever_username"];
        $model = new Message();
        $model->attributes = $_POST["Message"];


		if (User::find()->where(['username'=>$reciever_username])
        	->andWhere(['is_archived' => 0])
            ->exists())
            {
            	$reciver =  User::find()->where(['username'=>$reciever_username])
		        ->andWhere(['is_archived' => 0])
		        ->one();

		        $model->to_user = $reciver->id ;

		        if ($model->save()) 
		        {
			        $response = array(
			            "error"=>0 ,
			            "user_id" => $model->from_user,
			            "error_msg" =>""
			            );
			            echo json_encode($response);
		        } 
		        else 
		        {
		            $response = array(
		                "error"=>1,
		                "user_id" => $model->from_user,
		                "error_msg" =>$model->getErrors()
		                );
		            echo json_encode($response);
		        } 

            }

        else {

        	$response = array(
	            "error"=>2 ,
	            "user_id" => $model->from_user,
	            "error_msg" =>"No user with this User Name exists "
	            );
	            echo json_encode($response);
        }

        
        
    }


    public function actionMyMessages()
    {
    	$user_id = $_POST['user_id'];

    	$messages['messages'] =  Message::find()->where(['to_user'=>$user_id])
        ->asArray()
        ->all();


        foreach ($messages['messages'] as &$message ) {
            $id = $message["from_user"];
            $user = User::find()->where(['id'=>$id])
            ->one();
            $message['from_user'] = $user->username ;
        }

        $messages['user_id'] = $user_id ;
        echo json_encode($messages);
    
    }

    public function actionMySentMessages()
    {
    	$user_id = $_POST['user_id'];

    	$messages['messages'] =  Message::find()->where(['from_user'=>$user_id])
        ->asArray()
        ->all();


        foreach ($messages['messages'] as &$message ) {
            $id = $message["to_user"];
            $user = User::find()->where(['id'=>$id])
            ->one();
            $message['to_user'] = $user->username ;
        }

        $messages['user_id'] = $user_id ;
        echo json_encode($messages);
    
    }


    public function actionViewMessage()
    {
        $message_id = $_POST['message_id'];
        $user_id = $_POST['user_id'];
        $message =  Message::find()->where(['id'=>$message_id])
        ->asArray()
        ->one();

        $msg =  Message::find()->where(['id'=>$message_id])
        ->one();

        $message['from_user']= $msg->fromUser->username ;
        $message['to_user']= $msg->toUser->username ;
        $message['user_id'] = $user_id ;
        echo json_encode($message);
    
    }


   

}