<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\City;
use backend\models\User;
use backend\models\Room;
use backend\models\Advertisement;
use backend\models\Message;
use backend\models\Country;
use backend\models\EducationalLevel;
use common\helpers\AppHelper;
use common\helpers\UserHelper;
use PHPMailer;
use smtp;
use backend\models\Apartments;
use yii\base\Exception;


/**
 * Web controller
 */
class WebController extends Controller
{

    public $enableCsrfValidation = false;

	public function actionHome()
    {
        if (!Yii::$app->session->has('userid')) {
            return $this->render('login');
        }
        else{
            $id = Yii::$app->session->get('userid');
            $messages_count = Message::find()->where(['to_user'=>$id])->count() ;
            $advertisments_count = Advertisement::find()->where(['user_id'=>$id])->count() ;
            return $this->render('home',['messages' => $messages_count,'ads'=>$advertisments_count]);
        }
    }


    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionSignup()
    {
        if(isset($_POST['User']))
        {
            $model = new User();
            $model->attributes = $_POST["User"];
            if ($model->save()) 
            {
                Yii::$app->session->set('userid', $model->id);
                $this->redirect(\Yii::$app->urlManager->createUrl("web/home"));
            } 
            else 
            {
                Yii::$app->session->setFlash('danger',$model->getErrors());
                $this->redirect(\Yii::$app->urlManager->createUrl("web/signup"));
            }
            //echo json_encode($response);
        }
        else{
            $nationalities = Country::find()->all() ;
            $education_level = EducationalLevel::find()->all() ;
            return $this->render('signup',['nationalities' => $nationalities,'education_level' => $education_level]);
        }
    }

    public function actionEdit()
    {
        if(isset($_POST['User']))
        {
            $id = Yii::$app->session->get('userid');
            $model = User::find()->where(['id'=>$id])
            ->andWhere(['is_archived' => 0])->one();
            $model->attributes = $_POST["User"];
            if ($model->save()) 
            {
                Yii::$app->session->setFlash('success',"Updated");
                $this->redirect(\Yii::$app->urlManager->createUrl("web/profile"));
            } 
            else 
            {
                Yii::$app->session->setFlash('danger',"All Fields Must Be filled");
                $this->redirect(\Yii::$app->urlManager->createUrl("web/edit"));
            }
            
        }

        else {
            $nationalities = Country::find()->all() ;
            $education_level = EducationalLevel::find()->all() ;
            $id = Yii::$app->session->get('userid');
            $user = User::find()->where(['id'=>$id])
            ->andWhere(['is_archived' => 0])->one();
            return $this->render('edit',['user' => $user,'nationalities' => $nationalities,'education_level' => $education_level]);
        }
    }

    public function actionProfile()
    {
        $id = Yii::$app->session->get('userid');
        $user = User::find()->where(['id'=>$id])
        ->andWhere(['is_archived' => 0])->one();
        return $this->render('profile',['user' => $user]);
    }


    public function actionMyAdds()
    {
        $id = Yii::$app->session->get('userid');
        $my_adds = Advertisement::find()->where(['user_id'=>$id])
        ->all();
        return $this->render('advertisements',['my_adds' => $my_adds]);
    }


    public function actionAllAdds()
    {
        $my_adds = Advertisement::find()->all();
        return $this->render('all-advertisements',['my_adds' => $my_adds]);
    }


    public function actionAddAdvertisment()
    {
        if(isset($_POST["Room"])){
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                // 
                $room = new Room();
                //$user_id = $_POST["user_id"] ;
                $user_id = Yii::$app->session->get('userid');
                $advertise = new Advertisement();
                $apartment = Apartments::find()->where(['current_owner_id'=>$user_id])->one();
                $room->attributes = $_POST["Room"];
                $room->apartment_id = $apartment->id;
                $room->rate = 4 ;
                $room->picture = "null" ;

                $room->is_reserved = 1 ;
                $advertise->attributes = $_POST["Advertisement"];
                if (!$room->save()) 
                {
                    $message = "Adding a Room Failed";
                    throw new Exception($message);
                   
                } 

                $advertise->room_id = $room->id ;
                $advertise->user_id = $user_id ;
                $advertise->expire_date = date('Y-m-d', strtotime("+30 days"));


                if (!$advertise->save()) 
                {
                    $message ="Adding Advertisment Failed";
                    throw new Exception($message);
                } 
                $transaction->commit();
                $this->redirect(\Yii::$app->urlManager->createUrl("web/my-adds"));
              }


            catch (Exception $e) 
            {
                Yii::$app->session->setFlash('danger',$e);
                return $this->render('add-advertisment');
            }

        }

        else {
            $user_id = Yii::$app->session->get('userid');

            if(Apartments::find()->where(['current_owner_id'=>$user_id])->one()){
                return $this->render('add-advertisment');    
            }

            else {
                Yii::$app->session->setFlash('danger',"You Don't Have any registered Apartment, please add one to be able to advertise");
                $user_id = Yii::$app->session->get('userid');
                $model = User::find()->where(['id'=>$user_id])
                ->andWhere(['is_archived' => 0])->one();
                $cities = City::find()->where(['country_id'=>$model->Nationality])->all() ;
                return $this->render('add-apartment',['cities' => $cities]);
            }
            
        }
        
        
        //echo json_encode($response);
    }


    public function actionLogin()
    {

        if(isset($_POST['email'])&& isset($_POST['password']) )
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            if (User::find()->where(['email' => $email])
            ->andWhere(['password' => $password])
            ->andWhere(['is_archived' => 0])
            ->exists())
            {
                $model = User::find()->where(['email' => $email])
                ->andWhere(['password' => $password])
                ->andWhere(['is_archived' => 0])
                ->one();

                $session = Yii::$app->session ;
                $session->open();
                $session->set('userid', $model->id);

                $this->redirect(\Yii::$app->urlManager->createUrl("web/home"));
            }
            else
            {
                Yii::$app->session->setFlash('danger',"Wrong Email or Password");
                return $this->render('login');
            }
        }

        else{
            return $this->render('login');
        }
        
    }

    public function actionLogout()
    {
        $session = Yii::$app->session->remove('userid');
        $this->redirect(Yii::$app->urlManager->createUrl(["web/home"]));
    }


    public function actionLegalDisclaimer()
    {
        return $this->render('legal-disclaimer');
    }

    
    public function actionAddApartment()
    {

        $user_id = Yii::$app->session->get('userid');
        
        $user = User::find()->where(['id'=>$user_id])
        ->andWhere(['is_archived' => 0])->one();

        $model = new Apartments();
        $model->attributes = $_POST["Apartments"];

        $model->current_owner_id = $user_id ;
        $model->country_id = $user->Nationality ;
        $model->picture = "null";
        if ($model->save()) 
        {
            Yii::$app->session->setFlash('success',"New Apartment is created successfully");
            $this->redirect(\Yii::$app->urlManager->createUrl("web/add-advertisment"));
        } 
        else 
        {
            Yii::$app->session->setFlash('danger',$model->getErrors());
           // print_r($model->getErrors()) ;
           // exit();
            //$this->redirect(\Yii::$app->urlManager->createUrl("web/signup"));
        }
       
    }
    


    /*
    public function actionMail()
    {
        $mail = new PHPMailer;
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'atem250@gmail.com';                 // SMTP username
        $mail->Password = 'asdfae';                           // SMTP password
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->From = 'atem250@gmail.com';
        $mail->FromName = 'Test phpmailer';
        $mail->addAddress('ibnomar94@yahoo.com');               // Name is optional

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Here is the subject';
        $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->Priority    = 1; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
        $mail->CharSet     = 'UTF-8';
        $mail->Encoding    = '8bit';
        $mail->Subject     = 'Test Email Using Gmail';
        $mail->ContentType = 'text/html; charset=utf-8\r\n';

        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }

    }

    */

   


}