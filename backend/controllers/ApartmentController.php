<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\City;
use backend\models\User;
use backend\models\Room;
use backend\models\Advertisement;
use backend\models\Apartments;
use yii\base\Exception;

/**
 * Apartment controller
 */

class ApartmentController extends Controller
{
    public $enableCsrfValidation = false;


    public function actionHasApartment()
    {
    	$user_id =  $_POST["user_id"] ;
        if(!Apartments::find()->where(['current_owner_id'=>$user_id])->one())
        {
        // return $this->render('add-advertisment');   
        $model = User::find()->where(['id'=>$user_id])
        ->andWhere(['is_archived' => 0])->one();
        $cities = City::find()->where(['country_id'=>$model->Nationality])->asArray()->all();
        $response ['has_apartment'] = 0;
        $response ['user_id'] = $user_id ;
        $response ['cities'] = $cities ;
        $size = sizeof($cities);
        
        echo json_encode($response);
        }

        else 
        {
            $cities = City::find()->where(['id'=> 0])->asArray()->all();
           
            //$cities [] = "4"  ;
        	$response = array(
                "has_apartment" => 1 ,
                "user_id"=> $user_id,
                "cities" => $cities ,
                "cities" => $cities ,
                );
            echo json_encode($response);
        }
    }



    public function actionAddApartment()
    {
    	$user_id = $user_id =  $_POST["user_id"] ;
        
        $user = User::find()->where(['id'=>$user_id])
        ->andWhere(['is_archived' => 0])->one();

        $model = new Apartments();
        $model->attributes = $_POST["Apartments"];

        $model->current_owner_id = $user_id ;
        $model->country_id = $user->Nationality ;
        $model->picture = "null";
        if ($model->save()) 
        {
           $response = array(
                "error" => 0 ,
                "user_id"=> $user_id,
                );
            echo json_encode($response);
        } 
        else 
        {
            $response = array(
                "error" => 1 ,
                "user_id"=> $user_id,
                "error_message" => $model->getErrors(),
                );
            echo json_encode($response);
        }
    }

   








}