<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\City;
use backend\models\User;
use backend\models\Room;
use backend\models\RoomUsers;
use backend\models\Message;
use backend\models\Advertisement;

use yii\base\Exception;

/**
 * Room controller
 */
class RoomController extends Controller
{

    public $enableCsrfValidation = false;

	public function actionIndex()
    {
        echo "Room Controller";
        exit();
    }

    public function actionGetAllRooms()
    {
        $response = [];
		$response = Room::find()->asArray()->all();
		echo json_encode($response);
    }


    public function actionGetRoomById()
    {
        $id = $_POST['id'];
        $response = Room::find()->where(['id'=>$_POST['id']])
        ->asArray()->one();
        echo json_encode($response);
    }


    public function actionRateRoom()
    {
        $user_id = $_POST['user_id'];
        $room_id = $_POST['room_id'];
        $rate = $_POST['rate'];
        $comment = $_POST['comment'];
        $avg_rate = 4 ;
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            // 
            $model = RoomUsers::find()->where(['user_id' => $user_id])
            ->andWhere(['room_id' => $room_id])
            ->one();

            $model->rate = $rate ;
            $model->comment = $comment ;
            if (!$model->save()) 
            {
                $message = $model->getErrors();
                throw new Exception($message);
               
            } 

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("SELECT AVG(rate)
            FROM room_users 
            WHERE room_users.room_id =" .$room_id);

            $avg_rate = (int)$command->queryAll()[0]['AVG(rate)'];

           //  echo $avg_rate;
           //  print_r($command->queryAll()[0]['AVG(rate)']);
           //  exit();


            $room = Room::find()->where(['id' => $room_id])->one();
            $room->rate = $avg_rate ;
            if (!$room->save()) 
            {
                $message = $room->getErrors();
                throw new Exception($message);
            } 


            $transaction->commit();

            $response = array(
                    "error" => 0 ,
                    "room_id" => $room_id,
                    "user_id"=> $user_id,
                    );
            echo json_encode($response);
        }


        catch (Exception $e) 
        {
            $transaction->rollBack();
            $response = $e->errorInfo[0];
            $response = array(
                    "error" => 1,
                    "error_message"=> $e,
                    );
            echo json_encode($response);
        }
        
    }


    public function actionReserveRoom()
    {
       // $user_id = $_POST['user_id'];
       // $room_id = $_POST['room_id'] ;
        $model = new RoomUsers();
        $model->attributes = $_POST["RoomUsers"];
        $model->rate = 5 ;
        $model->comment = "intial" ;
        $room = Room::find()->where(['id'=>$model->room_id])->one();
        $model->date_from = $room->available_from ;
        $model->date_to = $room->available_to ;

        $date_now = date("Y-m-d"); 
        if ($date_now < $model->date_from )
        {
            $model->is_reserve = 1 ;
        }
        else
        {
            //echo '2oda m3adha fat';
            $model->is_reserve = 0 ;
        }

        if(!$rooms = RoomUsers::find()->where(['user_id'=>$model->user_id])
            ->andWhere(['room_id' => $model->room_id])
            ->exists())
        {
            // $reserver = $_POST['user_id'];
            // $adv_id = $_POST['adv_id'];
            $adverise = Advertisement::find()->where(['room_id' => $room->id])->one();
            $message = new Message();
            $message->from_user = $model->user_id ;
            $message->to_user = $adverise->user_id ;
            $message->subject = "Your room has Been reserved" ;
            $message->content = "Your room that is in ".$room->apartment->address." is now reserved by ".
            $model->user->username ;

            if ($model->save() && $message->save()) 
            {
                $response = array(
                    "error"=>0 ,
                    "user_id" => $model->user_id,
                    "room_id" => $model->room_id,
                    "error_msg" =>""
                    );
                    echo json_encode($response);
            } 
            else 
            {
                $response = array(
                    "error"=>1,
                    "user_id" => $model->user_id,
                    "room_id" =>0,
                    "error_msg" =>$model->getErrors()
                    );
                echo json_encode($response);
            } 
        }

        else
        {
            $response = array(
                    "error"=>1,
                    "user_id" => $model->user_id,
                    "room_id" =>0,
                    "error_msg" =>"You have reserved This room before"
                    );
                echo json_encode($response);
        }
        




        //echo json_encode($response);
    }


    public function actionAddRoom()
    {
        $model = new Room();
        $model->attributes = $_POST["Room"];
        if ($model->save()) 
        {
            $response = array(
                    "room_id" => $model->id,
                    "error"=>0
                    );
                    echo json_encode($response);
        } 
        else 
        {
            $response = array(
                    "room_id" =>0,
                    "error"=>1,
                    "error_msg" =>$model->getErrors()
                    );
            echo json_encode($response);
        }
        //echo json_encode($response);
    }


    public function actionEditRoom()
    {
        $id = $_GET['id'];
        $model = Room::find()->where(['id' => $id])->one();
        if(isset($_POST["Room"]))
        {
            $model->attributes = $_POST["Room"];
            if ($model->save()) 
            {
                $response = Room::find()->where(['id'=>$id])
                ->asArray()->one();
                echo json_encode($response);
            } 
            else 
            {
                echo json_encode($model->getErrors());
            }
        }
        
    }


   public function actionReserveNotify()
   {
        $reserver = $_POST['user_id'];
        $adv_id = $_POST['adv_id'];
        $adverise = Advertisement::find()->where(['id' => $adv_id])->one();
        $message = new Message();
        $room = Room::find()->where(['id' => $adverise->room_id])->one();
        $reserver_user = User::find()->where(['id' => $reserver])->one();
        $message->from_user = $reserver ;
        $message->to_user = $adverise->user_id ;
        $message->subject = "Your room has Been reserved" ;
        $message->content = "Your room that is in ".$room->apartment->address." is now reserved by ".
        $reserver_user->username ;
        if ($message->save()) 
        {
            $response = array(
                "error"=>0 ,
                "user_id" => $message->from_user,
                "error_msg" =>""
                );
                echo json_encode($response);
        } 
        else 
        {
            $response = array(
                "error"=>1,
                "user_id" => $message->from_user,
                "error_msg" =>$message->getErrors()
                );
            echo json_encode($response);
        } 
   }

}