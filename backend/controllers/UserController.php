<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\City;
use backend\models\Country;
use backend\models\User;
use backend\models\RoomUsers;
use backend\models\Room;
use backend\models\Advertisement;
use backend\models\UserRating;
use backend\models\Message;

/**
 * User controller
 */
class UserController extends Controller
{

    public $enableCsrfValidation = false;

	public function actionIndex()
    {
        echo "User Controller";
        exit();
    }


    public function actionLogin()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];
        if (User::find()->where(['email' => $email])
            ->andWhere(['password' => $password])
            ->andWhere(['is_archived' => 0])
            ->exists())
        {
            $model = User::find()->where(['email' => $email])
            ->andWhere(['password' => $password])
            ->andWhere(['is_archived' => 0])
            ->one();
            $session = Yii::$app->session ;
            $session->open();
            $session->set('userid', $model->id);

            $response = array(
                "user_id" => $model->id,
                "error"=>0
                );
            echo json_encode($response);
        }
        else
        {
            $response = array(
                    "user_id" => 0,
                    "error"=>1
                    );
                    echo json_encode($response);
        }
        
    }


    public function actionLogout()
    {
        $session = Yii::$app->session->remove('userid');
        $response = array(
                    "logged"=>0
                    );
        echo json_encode($response);
    }


    public function actionGetUserByIdLogged()
    {
        if (!Yii::$app->session->has('userid')) {
            $response = array(
                    "logged"=>0
                    );
            echo json_encode($response);
        }

        else {
            $response = array(
                    "logged"=>1
                    );
            echo json_encode($response);   
        }
    }



    public function actionGetAllUsers()
    {
        $response = [];
		$response['user'] = User::find()->where(['is_archived'=>0])->asArray()->all();
		echo json_encode($response);
    }


    public function actionGetAllCountries()
    {
        $response = [];
        $response ['Countries'] = Country::find()->asArray()->all();
        echo json_encode($response);
    }


    public function actionGetUserById()
    {
        //$id = $_POST['id'];
        $id = Yii::$app->session->get('userid');
        $response = User::find()->where(['id'=>$id])
        ->andWhere(['is_archived' => 0])->asArray()
        ->one();
        $Nationality = Country::find()->where(['id' => $response['Nationality'] ])->one();
        $response['Nationality'] = $Nationality->name ;
        echo json_encode($response);
    }


    public function actionGetUser()
    {
        $id = $_POST['id'];
        //$id = Yii::$app->session->get('userid');
        $response = User::find()->where(['id'=>$id])
        ->andWhere(['is_archived' => 0])->asArray()
        ->one();
        $Nationality = Country::find()->where(['id' => $response['Nationality'] ])->one();
        $response['Nationality'] = $Nationality->name ;
        echo json_encode($response);
    }


    public function actionGetUserByName()
    {
        $response = User::find()->where(['fname'=>$_GET['fname']])
        ->andWhere(['lname' => $_GET['lname']])
        ->andWhere(['is_archived' => 0])
        ->asArray()->one();
        echo json_encode($response);
    }


    public function actionAddUser()
    {
        $model = new User();
        $model->attributes = $_POST["User"];
        if ($model->save()) 
        {
            $response = array(
                    "user_id" => $model->id,
                    "error"=>0
                    );
                    echo json_encode($response);
        } 
        else 
        {
            $response = array(
                    "user_id" =>0,
                    "error"=>1,
                    "error_msg" =>$model->getErrors()
                    );
            echo json_encode($response);
        }
        //echo json_encode($response);
    }


    public function actionEditUser()
    {
        $id = $_POST['id'];
        if(isset($_POST["User"]))
        {
            $model = User::find()->where(['id' => $id])->one();
            $model->attributes = $_POST["User"];
            $base=$_REQUEST['User'][' picture'];
            $binary=base64_decode($base);
            header('Content-Type: bitmap; charset=utf-8');
            $file = fopen('uploaded_image'.$model->id.'.jpg', 'wb');
            fwrite($file, $binary);
            fclose($file);
            $model->picture = "null" ;
            if ($model->save()) 
            {
                $response = array(
                    "user_id" => $model->id,
                    "error"=>0
                    );
                echo json_encode($response);
            } 
            else 
            {
                $response = array(
                    "user_id" =>0,
                    "error"=>1,
                    "error_msg" =>$model->getErrors()
                    );
                echo json_encode($response);
            }
        }


        else{

            $response = User::find()->where(['id'=>$id])
            ->andWhere(['is_archived' => 0])->asArray()
            ->one();
            echo json_encode($response);
        }
        
    }


    public function actionDeleteUser()
    {
        $id = $_GET['id'];
        $model = User::find()->where(['id' => $id])->one();
        $model->is_archived = 1 ;
        if ($model->save()) 
        {
            $this->redirect(\Yii::$app->urlManager->createUrl("user/get-all-users"));
        } 
        else 
        {
            echo json_encode($model->getErrors());
        }
        
    }


    public function actionResetPassword()
    {
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        if (User::find()->where(['email' => $email])
        ->andWhere(['phone' => $phone])
        ->andWhere(['is_archived' => 0])
        ->exists())
        {
            $model = User::find()->where(['email' => $email])
            ->andWhere(['phone' => $phone])
            ->andWhere(['is_archived' => 0])
            ->one();
            $model->password = $model->username . "123456";
            if ($model->save()) 
            {
                $response = array(
                "error"=>0,
                "error_message"=>"No error",
                "user_id" => $model->id,
                );
                echo json_encode($response);  
            }
            else
            {
                $response = array(
                "error"=>1,
                "error_message"=>"Try again... Your password isn't restored",
                "user_id" => 0,
                );
                echo json_encode($response);  
            }
            
        }
        else 
        {
            $response = array(
                "error"=>2,
                "error_message"=>"Data You entered doesn't match.",
                "user_id" => 0,
                );
            echo json_encode($response);  
        }
        

        
    }


    public function actionUserRooms()
    {
        $user_id = $_POST['user_id'];
        $rooms = RoomUsers::find()->where(['user_id'=>$user_id])->asArray()->all();
        
        foreach ($rooms as &$room ) {
            $id = $room["room_id"];
            $rm = Room::find()->where(['id'=>$id])
            ->one();
            $room['address'] = $rm->apartment->address ;
            $room['used_as'] = $rm->used_as ;
           // $room['city'] = $rm->apartment->city->name ;
        }
        $response = array(
            "error"=>0,
            "rooms" => $rooms,
            "user_id" => $user_id,


            );
            echo json_encode($response); 
    }


    public function actionUserMates()
    {
        $user_id = $_POST['user_id'];
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT *
        FROM user
        WHERE user.id IN (
        SELECT room_users.user_id
        from room_users
        JOIN room
        ON room_users.room_id = room.id

        WHERE room.apartment_id IN ( SELECT DISTINCT apartments.id
        from apartments
        JOIN room
        ON room.apartment_id = apartments.id
        JOIN room_users
        ON room_users.room_id = room.id
        WHERE room_users.user_id = " .$user_id. " ) ) AND user.id != ".$user_id."");

        $result ['mates'] = $command->queryAll();

        $result ['user_id'] = $user_id ;
        echo json_encode($result);
    }


    public function actionRateUser()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            if (UserRating::find()->where(['rater_id' => $_POST["UserRating"]['rater_id']])
            ->andWhere(['rated_id' =>$_POST["UserRating"]['rated_id']])
            ->exists())
            {

                $old_record = UserRating::find()->where(['rater_id' => $_POST["UserRating"]['rater_id']])
                ->andWhere(['rated_id' =>$_POST["UserRating"]['rated_id']])
                ->one();
                $old_record->delete();

            }

            $model = new UserRating();
            $model->attributes = $_POST["UserRating"];
            if (!$model->save()) 
            {
                $message = $model->getErrors();
                throw new Exception($message);
               
            } 

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("SELECT AVG(rate)
            FROM user_rating 
            WHERE user_rating.rated_id =" .$model->rated_id);

            $avg_rate = (int)$command->queryAll()[0]['AVG(rate)'];

           //  echo $avg_rate;
           //  print_r($command->queryAll()[0]['AVG(rate)']);
           //  exit();


            $user = User::find()->where(['id' => $model->rated_id])->one();
            $user->rate = $avg_rate ;
            if (!$user->save()) 
            {
                $message = $user->getErrors();
                throw new Exception($message);
            } 


            $transaction->commit();

            $response = array(
                    "error" => 0 ,
                    "rated_id" => $model->rated_id,
                    "user_id"=> $model->rater_id,
                    );
            echo json_encode($response);
        }


        catch (Exception $e) 
        {
            $transaction->rollBack();
            $response = $e->errorInfo[0];
            $response = array(
                    "error" => 1,
                    "error_message"=> $e,
                    );
            echo json_encode($response);
        }
        
    }


    public function actionUserStats()
    {
        $id = $_POST['user_id'];
        $date_now = date("Y-m-d"); 
        $response['messages_count'] = Message::find()->where(['to_user'=>$id])->count() ;
        $response['advertisments_count'] = Advertisement::find()->where(['user_id'=>$id])->count() ;
        $response['active_advertisments'] = Advertisement::find()
        ->where(['user_id'=>$id])
        ->andWhere(['>', 'expire_date', $date_now])
        ->count() ;
        $response['messages_count'] = "You Have ".$response['messages_count']." Messages" ;
        $response['advertisments_count'] = "You Made ".$response['advertisments_count']." Advertisements" ;
        $response['active_advertisments'] = "You Have ".$response['active_advertisments']." Active Advertisements" ;
        echo json_encode($response);

    }



   


}