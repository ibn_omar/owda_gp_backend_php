<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\City;
use backend\models\User;
use backend\models\Room;
use backend\models\Advertisement;
use backend\models\Apartments;
use yii\base\Exception;

/**
 * Advertisement controller
 */

class AdvertisementController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        echo "Advertisement Controller";
        exit();
    }

    public function actionGetAllAds()
    {
        $response = [];
        $response ['ads'] = Advertisement::find()->asArray()->all();
        echo json_encode($response);
   
    }

    public function actionGetUserAds()
    {
        $id = $_POST["user_id"] ;
        //$response = [];
        //$response = Advertisement::find()->where(['user_id'=>$id])->asArray()->one();
        $response ['ads'] = Advertisement::find()
        ->joinWith('room')
        ->where(['user_id'=>$id])
        //->andWhere(['id' => "room_id"])
        ->asArray()->all();
        echo json_encode($response);
   
    }


    public function actionGetFilteredAds()
    {
        if (isset($_POST["price"])){

            $price = $_POST["price"] ;
            $city_id = $_POST["city_id"] ;
            $user_id = $_POST["user_id"] ; 

            $response ['ads'] = Room::find()
            ->joinWith('advertisements')
            ->joinWith('apartment')
            ->where(['city_id' => $city_id])
            ->andWhere(['<=', 'price', $price])    
            ->asArray()->all();
            $response['user_id'] = $user_id ;
            echo json_encode($response);

        }

        else {
            
            $nationality ;
            if (isset($_POST["user_id"])){
                $user_id = $_POST["user_id"] ; 
                $model = User::find()->where(['id'=>$user_id])
                ->andWhere(['is_archived' => 0])->one();

                $nationality = $model->Nationality;
            }


            else if (isset($_POST["country_id"])){

                $nationality = $_POST['country_id'];
            }
            $cities = City::find()->where(['country_id'=>$nationality])->asArray()->all(); ;
            $response ['cities'] = $cities ;
            echo json_encode($response); 
        }
   
    }


    public function actionAddNewAdvert()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try
        {
            // 
            $room = new Room();
            $user_id = $_POST["user_id"] ;
            //$user_id = Yii::$app->session->get('userid');
            $advertise = new Advertisement();
            $apartment = Apartments::find()->where(['current_owner_id'=>$user_id])->one();
            $room->attributes = $_POST["Room"];
            $room->apartment_id = $apartment->id;
            $room->rate = 4 ;
            $room->is_reserved = 1 ;
            $advertise->attributes = $_POST["Advertisement"];
            if (!$room->save()) 
            {
                $message = $room->getErrors();
                throw new Exception($message);
               
            } 

            $advertise->room_id = $room->id ;
            $advertise->user_id = $user_id ;
            $advertise->expire_date = date('Y-m-d', strtotime("+30 days"));


            if (!$advertise->save()) 
            {
                $message = $advertise->getErrors();
                throw new Exception($message);
            } 
            $transaction->commit();

            $response = array(
                    "error" => 0 ,
                    "room_id" => $room->id,
                    "adv_id"=> $advertise->id,
                    );
            echo json_encode($response);
        }


        catch (Exception $e) 
        {
            $transaction->rollBack();
            $response = $e->errorInfo[2];
            $response = array(
                    "error" => 1,
                    "error_message"=> $e,
                    );
            echo json_encode($response);
        }
        
        //echo json_encode($response);
    }


    public function actionEditAdvert()
    {
        $add_id = $_POST['add_id'];
        if(isset($_POST["Room"]) && isset($_POST["Advertisement"]))
        {
           // $room_id = $_POST['room_id'];
            $add_id = $_POST["add_id"] ;
            $transaction = Yii::$app->db->beginTransaction();
            try
            {
                //
                $advertise = Advertisement::find()->where(['id' => $add_id])->one(); 
                $room_id = $advertise->room_id ;
                $room = Room::find()->where(['id' => $room_id])->one();
                //$user_id = Yii::$app->session->get('userid');
                
                //$apartment = Apartments::find()->where(['current_owner_id'=>$user_id])->one();
                $room->attributes = $_POST["Room"];
                $advertise->attributes = $_POST["Advertisement"];
                if (!$room->save()) 
                {
                    $message = $room->getErrors();
                    throw new Exception($message);
                   
                } 

                if (!$advertise->save()) 
                {
                    $message = $advertise->getErrors();
                    throw new Exception($message);
                } 
                $transaction->commit();

                $response = array(
                        "error" => 0 ,
                        "room_id" => $room->id,
                        "adv_id"=> $advertise->id,
                        "user_id"=> $advertise->user_id,
                        );
                echo json_encode($response);
            }


            catch (Exception $e) 
            {
                $transaction->rollBack();
                $response = $e->errorInfo[2];
                $response = array(
                        "error" => 1,
                        "error_message"=> $e,
                        );
                echo json_encode($response);
            }
        }


        else{

            /*$response = Advertisement::find()
            ->where(['advertisement.id'=>$add_id])
            ->joinWith('room')
            
            //->andWhere(['id' => "room_id"])
            ->asArray()->all();
            echo json_encode($response);*/
            
            $response = Advertisement::find()
            ->where(['advertisement.id'=>$add_id])
            ->joinWith('room')
            //->andWhere(['id' => "room_id"])
            ->asArray()->one();
            $User = User::find()->where(['id' => $response['user_id'] ])->one();
            $response['user_id'] = $User->username ;
            echo json_encode($response);
        }
        
    }

}

