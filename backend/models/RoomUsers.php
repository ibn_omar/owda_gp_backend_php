<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "room_users".
 *
 * @property integer $room_id
 * @property integer $user_id
 * @property integer $rate
 * @property string $comment
 * @property integer $is_reserve
 * @property string $date_from
 * @property string $date_to
 *
 * @property Room $room
 * @property User $user
 */
class RoomUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_id', 'user_id', 'rate', 'comment', 'is_reserve', 'date_from', 'date_to'], 'required'],
            [['room_id', 'user_id', 'rate', 'is_reserve'], 'integer'],
            [['comment'], 'string'],
            [['date_from', 'date_to'], 'safe'],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'user_id' => 'User ID',
            'rate' => 'Rate',
            'comment' => 'Comment',
            'is_reserve' => 'Is Reserve',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
