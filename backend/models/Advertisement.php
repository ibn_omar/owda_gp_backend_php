<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "advertisement".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $user_id
 * @property integer $room_id
 * @property integer $price
 * @property integer $inadvance_pay
 * @property integer $including_maintenance
 * @property string $expire_date
 *
 * @property User $user
 * @property Room $room
 */
class Advertisement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'user_id', 'room_id', 'price', 'expire_date'], 'required'],
            [['body'], 'string'],
            [['user_id', 'room_id', 'price', 'inadvance_pay', 'including_maintenance'], 'integer'],
            [['expire_date'], 'safe'],
            [['title'], 'string', 'max' => 200],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['room_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'user_id' => 'User ID',
            'room_id' => 'Room ID',
            'price' => 'Price',
            'inadvance_pay' => 'Inadvance Pay',
            'including_maintenance' => 'Including Maintenance',
            'expire_date' => 'Expire Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }
}
