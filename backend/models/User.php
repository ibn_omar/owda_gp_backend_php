<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $fname
 * @property string $lname
 * @property resource $picture
 * @property string $username
 * @property string $password
 * @property string $phone
 * @property string $email
 * @property integer $Nationality
 * @property string $fb_account
 * @property integer $educational_level
 * @property integer $age
 * @property integer $gender
 * @property integer $rate
 * @property integer $marital_status
 * @property integer $is_smoker
 * @property integer $is_archived
 *
 * @property Advertisement[] $advertisements
 * @property Apartments[] $apartments
 * @property Message[] $messages
 * @property Message[] $messages0
 * @property RoomUsers[] $roomUsers
 * @property Room[] $rooms
 * @property Country $nationality
 * @property UserRating[] $userRatings
 * @property UserRating[] $userRatings0
 * @property User[] $rateds
 * @property User[] $raters
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fname', 'lname', 'username', 'password', 'phone', 'email', 'Nationality', 'age'], 'required'],
            [['picture'], 'string'],
            [['Nationality', 'educational_level', 'age', 'gender', 'rate', 'marital_status', 'is_smoker', 'is_archived'], 'integer'],
            [['fname', 'lname', 'username', 'password', 'phone', 'email', 'fb_account'], 'string', 'max' => 200],
            [['phone'], 'unique'],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [['Nationality'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['Nationality' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'picture' => 'Picture',
            'username' => 'Username',
            'password' => 'Password',
            'phone' => 'Phone',
            'email' => 'Email',
            'Nationality' => 'Nationality',
            'fb_account' => 'Fb Account',
            'educational_level' => 'Educational Level',
            'age' => 'Age',
            'gender' => 'Gender',
            'rate' => 'Rate',
            'marital_status' => 'Marital Status',
            'is_smoker' => 'Is Smoker',
            'is_archived' => 'Is Archived',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisements()
    {
        return $this->hasMany(Advertisement::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartments()
    {
        return $this->hasMany(Apartments::className(), ['current_owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['from_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages0()
    {
        return $this->hasMany(Message::className(), ['to_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomUsers()
    {
        return $this->hasMany(RoomUsers::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['id' => 'room_id'])->viaTable('room_users', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNationality()
    {
        return $this->hasOne(Country::className(), ['id' => 'Nationality']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRatings()
    {
        return $this->hasMany(UserRating::className(), ['rater_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRatings0()
    {
        return $this->hasMany(UserRating::className(), ['rated_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRateds()
    {
        return $this->hasMany(User::className(), ['id' => 'rated_id'])->viaTable('user_rating', ['rater_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaters()
    {
        return $this->hasMany(User::className(), ['id' => 'rater_id'])->viaTable('user_rating', ['rated_id' => 'id']);
    }
}
