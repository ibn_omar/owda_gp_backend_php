<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "apartments".
 *
 * @property integer $id
 * @property integer $current_owner_id
 * @property integer $ownership_type
 * @property integer $country_id
 * @property integer $city_id
 * @property string $address
 * @property integer $floor_num
 * @property integer $num_of_rooms
 * @property string $nearist_metro
 * @property integer $max_inhabitants
 * @property integer $current_inhabitants
 * @property integer $has_kitchen
 * @property integer $separate_bathrooms
 * @property integer $has_elevator
 * @property integer $pets_allowance
 * @property integer $smoking_allowance
 * @property integer $has_wifi
 * @property integer $gender_preferences
 * @property integer $furniture_status
 * @property string $picture
 *
 * @property User $currentOwner
 * @property Country $country
 * @property City $city
 * @property Room[] $rooms
 */
class Apartments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'apartments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['current_owner_id', 'ownership_type', 'country_id', 'city_id', 'address', 'num_of_rooms', 'picture'], 'required'],
            [['current_owner_id', 'ownership_type', 'country_id', 'city_id', 'floor_num', 'num_of_rooms', 'max_inhabitants', 'current_inhabitants', 'has_kitchen', 'separate_bathrooms', 'has_elevator', 'pets_allowance', 'smoking_allowance', 'has_wifi', 'gender_preferences', 'furniture_status'], 'integer'],
            [['address', 'nearist_metro', 'picture'], 'string', 'max' => 200],
            [['address'], 'unique'],
            [['current_owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['current_owner_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'current_owner_id' => 'Current Owner ID',
            'ownership_type' => 'Ownership Type',
            'country_id' => 'Country ID',
            'city_id' => 'City ID',
            'address' => 'Address',
            'floor_num' => 'Floor Num',
            'num_of_rooms' => 'Num Of Rooms',
            'nearist_metro' => 'Nearist Metro',
            'max_inhabitants' => 'Max Inhabitants',
            'current_inhabitants' => 'Current Inhabitants',
            'has_kitchen' => 'Has Kitchen',
            'separate_bathrooms' => 'Separate Bathrooms',
            'has_elevator' => 'Has Elevator',
            'pets_allowance' => 'Pets Allowance',
            'smoking_allowance' => 'Smoking Allowance',
            'has_wifi' => 'Has Wifi',
            'gender_preferences' => 'Gender Preferences',
            'furniture_status' => 'Furniture Status',
            'picture' => 'Picture',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'current_owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['apartment_id' => 'id']);
    }
}
