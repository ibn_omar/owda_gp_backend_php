<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_rating".
 *
 * @property integer $rater_id
 * @property integer $rated_id
 * @property integer $rate
 * @property string $comment
 *
 * @property User $rater
 * @property User $rated
 */
class UserRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rater_id', 'rated_id', 'rate', 'comment'], 'required'],
            [['rater_id', 'rated_id', 'rate'], 'integer'],
            [['comment'], 'string'],
            [['rater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rater_id' => 'id']],
            [['rated_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rated_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rater_id' => 'Rater ID',
            'rated_id' => 'Rated ID',
            'rate' => 'Rate',
            'comment' => 'Comment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRater()
    {
        return $this->hasOne(User::className(), ['id' => 'rater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRated()
    {
        return $this->hasOne(User::className(), ['id' => 'rated_id']);
    }
}
