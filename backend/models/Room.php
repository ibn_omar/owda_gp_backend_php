<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "room".
 *
 * @property integer $id
 * @property integer $apartment_id
 * @property integer $num_of_beds
 * @property integer $available_beds
 * @property string $used_as
 * @property integer $has_bathroom
 * @property integer $has_balcony
 * @property integer $rate
 * @property integer $is_reserved
 * @property string $available_from
 * @property string $available_to
 * @property string $picture
 *
 * @property Advertisement[] $advertisements
 * @property Apartments $apartment
 * @property RoomUsers[] $roomUsers
 * @property User[] $users
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apartment_id', 'num_of_beds', 'available_beds', 'is_reserved', 'picture'], 'required'],
            [['apartment_id', 'num_of_beds', 'available_beds', 'has_bathroom', 'has_balcony', 'rate', 'is_reserved'], 'integer'],
            [['available_from', 'available_to'], 'safe'],
            [['used_as'], 'string', 'max' => 30],
            [['picture'], 'string', 'max' => 200],
            [['apartment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Apartments::className(), 'targetAttribute' => ['apartment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apartment_id' => 'Apartment ID',
            'num_of_beds' => 'Num Of Beds',
            'available_beds' => 'Available Beds',
            'used_as' => 'Used As',
            'has_bathroom' => 'Has Bathroom',
            'has_balcony' => 'Has Balcony',
            'rate' => 'Rate',
            'is_reserved' => 'Is Reserved',
            'available_from' => 'Available From',
            'available_to' => 'Available To',
            'picture' => 'Picture',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisements()
    {
        return $this->hasMany(Advertisement::className(), ['room_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartments::className(), ['id' => 'apartment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomUsers()
    {
        return $this->hasMany(RoomUsers::className(), ['room_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('room_users', ['room_id' => 'id']);
    }
}
