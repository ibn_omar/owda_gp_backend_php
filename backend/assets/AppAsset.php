<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '../assets/plugins/pace/themes/pace-theme-flash.css',
        'web/css/site.css',
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        '../assets/plugins/font-awesome/css/font-awesome.min.css',
        '../assets/plugins/simple-line-icons/simple-line-icons.min.css',
        '../assets/plugins/bootstrap/css/bootstrap.min.css',
        '../assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        '../assets/plugins/datatables/datatables.min.css',
        '../assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

        
        
        '../assets/plugins/introJS/introjs.css',
        '../assets/plugins/selectize/dist/css/selectize.bootstrap3.css',

        '../assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
        '../assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
        '../assets/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
        '../assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        '../assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        '../assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
        '../assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
    ];
    public $js = [
        '../assets/plugins/pace/pace.min.js',
        '../assets/plugins/jquery.min.js',
        '../assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
        '../assets/plugins/bootstrap/js/bootstrap.min.js',
        '../assets/plugins/js.cookie.min.js',
        '../assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        '../assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        '../assets/plugins/jquery.blockui.min.js',
        '../assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        

        '../assets/plugins/datatables/datatables.min.js',
        '../assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',

        '../assets/plugins/introJS/intro.js',



        '../assets/plugins/moment.min.js',
        '../assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js',
        '../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        '../assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

        '../assets/plugins/selectize/dist/js/standalone/selectize.js',
        '../assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        '../assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        '../assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        '../assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',


    ];
    //to load javascript in header 

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
